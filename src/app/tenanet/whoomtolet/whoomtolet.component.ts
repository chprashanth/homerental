import { Component, OnInit, OnChanges } from '@angular/core';
import { StatusService } from 'src/app/status.service';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-whoomtolet',
  templateUrl: './whoomtolet.component.html',
  styleUrls: ['./whoomtolet.component.css']
})
export class WhoomtoletComponent implements OnInit {
  b: boolean = true;
  status: boolean;
  check: boolean;
  value: string;
  house: any;
  currentuser: any;
  searchTerm: any;
  constructor(private r: StatusService, private hc: HttpClient, private cm: RegisterService) { }

  //requestdata()
  //{
  //if(this.status===true)
  // {
  //this.value='request accepted';
  //}
  //else if(this.status===false)
  // {
  // this.value='request rejected';
  // }
  //}
  ngOnInit() {
    //this.status=this.r.status;
    // this.check=this.r.check;
    this.currentuser = this.cm.currentuser[0]
    this.hc.get('/home/whoomtolet').subscribe(res => {
      if (res['message'] == 'unauthorized access') {
        alert(res['message'])
      }
      else {
        this.house = res['message']
      }

    })
  }
  ngOnchanges() {
  }
  Changestatus(expectedRent, house) {
    // console.log(expectedRent,house);
    // console.log(this.currentuser);
    var intrestedHouse = {
      "eRent": expectedRent,
      "address": house.address,
      "vendorname": this.currentuser.username,
      "number": this.currentuser.number,
      "email": this.currentuser.email,
      "ownername": house.username
    }
    //console.log(intrestedHouse)
    this.hc.post('/home/whoomtolet', intrestedHouse).subscribe(res => {
      alert(res['message'])
    })
    //this.b=false;
    // this.r.whoomtolet=this.b;
  }
}