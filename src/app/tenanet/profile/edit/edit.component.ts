import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/register.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  currentuser: any;
  constructor(private http: HttpClient, private cm: RegisterService, private router: Router) { }

  ngOnInit() {
    this.currentuser = this.cm.currentuser[0]
    console.log(this.cm.currentuser)
  }
  edit1(data) {
    if (data.number == "" || data.upassword == "") {
      alert("password and mobileno fields are mandatory")
    }
    else {
      this.http.put('home/edit', data).subscribe(res => {
        alert(res["message"])
        this.router.navigate(['/home/viprofile'])
      })
    }
  }
}
