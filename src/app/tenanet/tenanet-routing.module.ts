import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PaymentsComponent } from './payments/payments.component';
import { ProfileComponent } from './profile/profile.component';
import { WhoomtoletComponent } from './whoomtolet/whoomtolet.component';
import { ViewComponent } from '../board/payments/view/view.component';
import { EditComponent } from './profile/edit/edit.component';
import { ViprofileComponent } from './profile/viprofile/viprofile.component';
import { DopayComponent } from './payments/dopay/dopay.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent,
  children: [
    { path: 'profile', component: ProfileComponent },
    { path: 'viprofile', component: ViprofileComponent },
    { path: 'edit', component: EditComponent },
    { path: 'whoomtolet', component: WhoomtoletComponent },
    { path: 'payments', component: PaymentsComponent },
    { path: 'dopay', component: DopayComponent },
    { path: 'paymenthistory', component: PaymenthistoryComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenanetRoutingModule { }
