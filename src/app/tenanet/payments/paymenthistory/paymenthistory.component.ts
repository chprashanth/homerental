import { Component, OnInit } from '@angular/core';
import { HouseService } from 'src/app/house.service';

@Component({
  selector: 'app-paymenthistory',
  templateUrl: './paymenthistory.component.html',
  styleUrls: ['./paymenthistory.component.css']
})
export class PaymenthistoryComponent implements OnInit {

  constructor(private http: HouseService) { }
  viewhistory: any[];
  ngOnInit() {
    this.http.history().subscribe(data => {
      this.viewhistory = data['message'];
    })
  }

}
