import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/register.service';
import { HouseService } from 'src/app/house.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {
  currentuser: any;
  constructor(private cm: RegisterService, private http: HttpClient) { }

  ngOnInit() {
    this.currentuser = this.cm.currentuser[0]
    //console.log(this.currentuser)
  }
}
