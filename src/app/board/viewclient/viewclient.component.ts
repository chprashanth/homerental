import { Component, OnInit } from '@angular/core';
import { StatusService } from 'src/app/status.service';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-viewclient',
  templateUrl: './viewclient.component.html',
  styleUrls: ['./viewclient.component.css']
})
export class ViewclientComponent implements OnInit {
  //whoomTolet:boolean;
  //check:boolean;
  clients: any;
  constructor(private hc: HttpClient, private register: RegisterService) { }
  ngOnInit() {
    this.hc.get(`/dashboard/viewclient/${this.register.currentuser[0].username}`).subscribe(res => {
      this.clients = (res["message"])
      console.log(this.clients)
    })
    //console.log(this.register.currentuser[0].username)
  }
  accept(data) {
    data.reqstatus = "Request Accepted";
    this.register.setResponse(data).subscribe(res => {
      alert('response sent')
      this.clients = res['data']

    })
  }
  reject(data) {
    data.reqstatus = "Request Rejected";
    this.register.setResponse(data).subscribe(res => {
      alert('response sent')
      this.clients = res['data']
    })
  }
}
