import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardRoutingModule } from './board-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { EditprofileComponent } from './profile/editprofile/editprofile.component';
import { NavprofileComponent } from './navprofile/navprofile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import { ViewhouseComponent } from './viewhouse/viewhouse.component';
import { ViewclientComponent } from './viewclient/viewclient.component';
import { PaymentsComponent } from './payments/payments.component';
import { NavComponent } from './payments/nav/nav.component';
import { AddComponent } from './payments/add/add.component';
import { ViewComponent } from './payments/view/view.component';
import { HistoryComponent } from './payments/history/history.component';
import { FormsModule } from '@angular/forms';
import { MyrequestsComponent } from './myrequests/myrequests.component';


@NgModule({
  declarations: [DashboardComponent, ProfileComponent, ViewprofileComponent, EditprofileComponent, NavprofileComponent, AddhouseComponent, ViewhouseComponent, ViewclientComponent, PaymentsComponent, NavComponent, AddComponent, ViewComponent, HistoryComponent, MyrequestsComponent],
  imports: [
    CommonModule,
    BoardRoutingModule,
    FormsModule
  ]
})
export class BoardModule { }
