const mongoclient = require('mongodb').MongoClient
const url = "mongodb+srv://prashanth:xxxxxx@cluster0-nb1oq.mongodb.net/test?retryWrites=true&w=majority";
var dbo;
function initDb() {
    mongoclient.connect(url, { useNewUrlParser: true }, (err, client) => {
        if (err) {
            console.log("error in connected..");
            console.log(err);
        }
        else {
            dbo = client.db('project');
            console.log("database connected successfully....");

        }
    });
}
function getDb() {
    //console.log(dbo,"db has not initialized,please call initDb() first");
    return dbo;
}

module.exports = {
    initDb,
    getDb
}