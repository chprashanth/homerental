//create owner dashboard routes
const exp = require('express');
var adminroutes = exp.Router();
//importing dbconfig file
const initDb = require('../DBconfig').initDb;
const getDb = require('../DBconfig').getDb;
initDb();
//importing post request for admin
adminroutes.post('/login', (req, res, next) => {
    console.log(req.body);
    var dbo = getDb();
    dbo.collection('admin').find({ name: { $eq: req.body.name } }).toArray((err, dataArray) => {
        if (dataArray.length == 0) {
            res.json({ message: "Invalid admin name" })
        }
        else {
            if (dataArray[0].name == req.body.name) {
                res.json({ message: "admin login successfully.." })
            }
            else {
                res.json({ message: "Invalid admin password" })
            }
        }
    })
});
//get request for admin 
adminroutes.get('/viewprofiles', (req, res) => {
    var dbo = getDb();
    dbo.collection('owner').find().toArray((err, dataArray) => {
        if (err) {
            console.log("error in reading data");
            console.log(err)
        }
        else {
            dbo.collection('vendor').find().toArray((err, dataArray1) => {
                if (err) {
                    console.log("error in reading data");
                    console.log(err)
                }
                else {
                    res.json({ data: dataArray, data1: dataArray1 })
                }
            })
        }
    })
})





module.exports = adminroutes;