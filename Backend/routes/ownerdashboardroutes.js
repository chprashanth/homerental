//create owner dashboard routes
const exp = require('express');
//import bcrypt module
const bcrypt = require('bcrypt');
//import checkauthorization middleware
const checkauthorization = require('../middleware/checkauthorization')
var ownerdashboardroutes = exp.Router();
//importing dbconfig file
const initDb = require('../DBconfig').initDb;
const getDb = require('../DBconfig').getDb;
initDb();
//import req handelers
//importing post request for addhouse
ownerdashboardroutes.post('/addhouse', checkauthorization, (req, res, next) => {
    console.log(req.body);
    dbo = getDb();
    if (req.body.length == 0) {
        res.json({ "message": "no data from client" });
    }
    else {
        req.body.status = true
        dbo.collection('houseCollection').find({ address: { $eq: req.body.address } }).toArray((err, dataArray) => {
            if (dataArray.length == 0) {
                dbo.collection('houseCollection').insertOne(req.body, (err, success) => {
                    if (err) {
                        console.log("error in save operation");
                        next(err);
                    }
                    else {
                        res.json({ "message": "successfully add houses..." })
                    }
                });
            }
            else {
                res.json({ message: "same address exists please change the address" })
            }
        })
    }
});
//import post request for addpayments
ownerdashboardroutes.post('/addpayments', checkauthorization, (req, res, next) => {
    console.log(req.body);
    dbo = getDb();
    if (req.body == {}) {
        res.json({ "message": "no data from client" });
    }
    else {
        dbo.collection('paymentsCollection').insertOne(req.body, (err, success) => {
            if (err) {
                console.log("error in save operation");
                next(err);
            }
            else {
                res.json({ "message": "successfully add payment details.." })
            }
        });
    }
});
//import get request for view houses
ownerdashboardroutes.get('/viewhouse/:username', checkauthorization, (req, res, next) => {
    dbo = getDb();
    dbo.collection('houseCollection').find({ username: { $eq: req.params.username } }).toArray((err, data) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ "message": data })
        }
    });
});
//import get request for viewpayment
ownerdashboardroutes.get('/viewpayments/:username', checkauthorization, (req, res, next) => {
    dbo = getDb();
    dbo.collection('paymentsCollection').find({ username: { $eq: req.params.username } }).toArray((err, data) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ "message": data })
        }
    });
});
//import get request for viewpaymenthistory
ownerdashboardroutes.get('/viewpaymenthistory/:username', checkauthorization, (req, res, next) => {
    console.log(req.params)
    dbo = getDb();
    dbo.collection('dopayCollection').find({ owner: { $eq: req.params.username } }).toArray((err, dataArray) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ data: dataArray })
        }
    });
});
//delete handeler
ownerdashboardroutes.delete('/delete/:address', checkauthorization, (req, res, next) => {
    console.log(req.params);
    //delete document with address
    dbo = getDb();
    dbo.collection('houseCollection').deleteOne({ address: { $eq: req.params.address } }, (err, success) => {
        if (err) {
            next(err);
        }
        else {
            //read remaining documents
            dbo.collection('houseCollection').find().toArray((err, dataArray) => {
                if (err) {
                    next(err);
                }
                else {
                    res.json({ message: "record delete", data: dataArray })
                }
            })
        }
    })
});
//PUT HANDLERS
ownerdashboardroutes.put('/editprofile', checkauthorization, (req, res, next) => {
    console.log(req.body);
    bcrypt.hash(req.body.upassword, 5, (err, hashedpassword) => {
        if (err) {
            console.log(err)
        }
        else {
            //replace plane text password to hashed password
            req.body.upassword = hashedpassword;
            dbo = getDb();
            dbo.collection('owner').updateOne({ username: { $eq: req.body.username } }, {
                $set: {
                    date: req.body.date, mail: req.body.mail, number: req.body.number,
                    address: req.body.address, upassword: req.body.upassword
                }
            }, (err, success) => {
                if (err) {
                    console.log("error in updating data..");
                    next(err);
                }
                else {
                    res.json({ "message": "updated successfully..." })
                }
            })
        }
    })
})
//view clients get request
ownerdashboardroutes.get('/viewclient/:username', checkauthorization, (req, res, next) => {
    dbo = getDb();
    console.log(req.params);
    dbo.collection('tolet').find({ ownername: { $eq: req.params.username } }).toArray((err, data) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ "message": data })
        }
    });
});
//update the request from client
// put method handler for update the owner 
ownerdashboardroutes.put('/viewclient', (req, res, next) => {
    console.log(req.body)
    var dbo = getDb();
    dbo.collection("houseCollection").updateOne({ address: { $eq: req.body.address } },
        { $set: { reqstatus: req.body.reqstatus, vendorname: req.body.vendorname } }, (err, success) => {
            if (err) {
                console.log('error in saving data')
                next(err)
            }
            else {
                if (req.body.reqstatus == 'Request Accepted') {
                    dbo.collection('request').insertOne(req.body, (err, success) => {
                        if (err) {
                            next(err)
                        }
                        else {
                            dbo.collection("tolet").deleteOne({ $and: [{ vendorname: req.body.vendorname }, { address: req.body.address }] },
                                (err, success) => {
                                    if (err) {
                                        console.log('error in saving data')
                                        next(err)
                                    }
                                    else {
                                        dbo.collection('tolet').find({ ownername: { $eq: req.body.ownername } }).toArray((err, dataArray) => {
                                            if (err) {
                                                next(err)
                                            }
                                            else {
                                                res.json({ data: dataArray })
                                            }
                                        })
                                    }
                                })
                        }
                    })
                }
                else {
                    dbo.collection('tolet').deleteOne({ $and: [{ vendorname: req.body.vendorname }, { address: req.body.address }] }, (err, success) => {
                        if (err) {
                            console.log('error in saving data')
                            next(err)
                        }
                        else {
                            dbo.collection('tolet').find({ ownername: { $eq: req.body.ownername } }).toArray((err, dataArray) => {
                                if (err) {
                                    next(err)
                                }
                                else {
                                    res.json({ data: dataArray })
                                }
                            })
                        }
                    })
                }


            }
        })
})
//my requests get request
ownerdashboardroutes.get('/myrequests/:username', checkauthorization, (req, res, next) => {
    var dbo = getDb();
    console.log(req.params);
    dbo.collection('request').find({ ownername: { $eq: req.params.username } }).toArray((err, dataArray) => {
        if (err) {
            next(err);
        }
        else {
            res.json({ data: dataArray })
        }
    });
});
//import error handling
ownerdashboardroutes.use((err, req, res, next) => {
    console.log(err);
})
module.exports = ownerdashboardroutes;