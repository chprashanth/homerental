(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/admin/admin.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/admin/admin.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/admin/login/login.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/admin/login/login.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <h2 class=\"text-center text-primary\">LOGIN FORM FOR ADMIN</h2>\n            <form #ref=\"ngForm\" (ngSubmit)='submit(ref.value)'>\n                <div class=\"form-group\">\n                    <label for=\"login\">UserName</label>\n                    <input type=\"text\" name=\"name\" class=\"form-control\" id=\"login\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"lpassword\">Password</label>\n                    <input type=\"password\" name=\"password\" class=\"form-control\" id=\"lpassword\" ngModel>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\" type=\"submit\">LOGIN</button>\n                </div>\n\n            </form>\n        </div>\n    </div>\n    <div class=\"col-md-3\"></div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/admin/viewprofiles/viewprofiles.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/admin/viewprofiles/viewprofiles.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive\">\n    <h4 class=\"text-center\">OWNER DETAILS</h4>\n    <table class=\"table\">\n        <thead class=\"thead-dark\">\n            <th>Name</th>\n            <th>DOB</th>\n            <th>Address</th>\n            <th>Mobileno</th>\n            <th>Mail</th>\n            <th>Usertype</th>\n        </thead>\n        <tr *ngFor='let obj of data'>\n            <td>{{obj.username}}</td>\n            <td>{{obj.date}}</td>\n            <td>{{obj.address}}</td>\n            <td>{{obj.number}}</td>\n            <td>{{obj.email}}</td>\n            <td>{{obj.user}}</td>\n        </tr>\n    </table>\n</div>\n<div class=\"table-responsive\">\n    <h4 class=\"text-center\">VENDOR DETAILS</h4>\n    <table class=\"table\">\n        <thead class=\"thead-dark\">\n            <th>Name</th>\n            <th>DOB</th>\n            <th>Address</th>\n            <th>Mobileno</th>\n            <th>Mail</th>\n            <th>Usertype</th>\n        </thead>\n        <tr *ngFor='let obj of data1'>\n            <td>{{obj.username}}</td>\n            <td>{{obj.date}}</td>\n            <td>{{obj.address}}</td>\n            <td>{{obj.number}}</td>\n            <td>{{obj.email}}</td>\n            <td>{{obj.user}}</td>\n        </tr>\n    </table>\n</div>\n<button class=\"btn btn-success\" (click)='logout()'>Logout</button>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/addhouse/addhouse.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/addhouse/addhouse.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <h3 class=\"text-danger bg-info\">House Type Details</h3>\n\n        <form #ref=\"ngForm\" (ngSubmit)='addhouse(ref.value)'>\n                <div class=\"form-group\">\n                        <label>HouseType:</label>*\n                        <input type=\"text\" name=\"house\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                        <label>Address:</label>*\n                        <textarea name=\"address\" id=\"\" rows=\"5\" class=\"form-control\" ngModel #ref1=\"ngModel\"\n                                required></textarea>\n                        <label *ngIf=\"ref1.invalid && ref1.touched\" class=\"text-danger\">*this field is mandatory</label>\n                </div>\n                <div class=\"form-group\">\n                        <label>Rules:</label>\n                        <textarea name=\"rules\" id=\"\" rows=\"5\" class=\"form-control\" ngModel></textarea>\n                </div>\n                <div class=\"form-group\">\n                        <label>Rent:</label>\n                        <input type=\"number\" name=\"rent\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                        <label>Image:</label>\n                        <input type=\"file\" name=\"image\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group text-center\">\n                        <button class=\"btn btn-primary\" type=\"submit\">ADD</button>\n                </div>\n        </form>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/dashboard/dashboard.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/dashboard/dashboard.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h3 class=\"text-success\">OWNER DASHBOARD</h3>\n    <div class=\"row\">\n        <div class=\"col-md-3\">\n            <div class=\"card text-center\">\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/profile\">Profile</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/addhouse\">Add house</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/viewhouse\">View house </a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/viewclient\">View client</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/myrequests\">MyRequests</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"/dashboard/payments\">Payments</a>\n                </div>\n                <div class=\"\">\n                    <button class=\"btn text-primary\" (click)='logout()'>Logout</button>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <router-outlet></router-outlet>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/myrequests/myrequests.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/myrequests/myrequests.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <table class=\"table\">\n                <thead>\n                    <th>vendorname</th>\n                    <th>address</th>\n                    <th>email</th>\n                    <th>number</th>\n                    <th>eRent</th>\n                    <th>Status</th>\n                </thead>\n                <tr *ngFor='let client of currentuser'>\n                    <td>{{client.vendorname}}</td>\n                    <td>{{client.address}}</td>\n                    <td>{{client.email}}</td>\n                    <td>{{client.number}}</td>\n                    <td>{{client.eRent}}</td>\n                    <td>{{client.reqstatus}}</td>\n                </tr>\n            </table>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/navprofile/navprofile.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/navprofile/navprofile.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav nav-expand-sm content-justify-center\">\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dashboard/viewprofile\">ViewProfile</a>\n    </li>\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dashboard/editprofile\">EditProfile</a>\n    </li>\n</ul>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/payments/add/add.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/payments/add/add.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <form #ref3=\"ngForm\" (ngSubmit)='addpay(ref3.value)'>\n        <div class=\"form-group\">\n            <label>Account Number:</label>*\n            <input type=\"number\" name=\"account\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n            <label *ngIf=\"ref1.invalid && ref1.touched\" class=\"text-danger\">*this field is mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>IFSC Code:</label>*\n            <input type=\"type\" name=\"ifsc\" class=\"form-control\" ngModel #ref2=\"ngModel\" required>\n            <label *ngIf=\"ref2.invalid && ref2.touched\" class=\"text-danger\">*this field is mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>GOOGLEPay:</label>\n            <input type=\"number\" name=\"pay\" class=\"form-control\" ngModel>\n        </div>\n        <div class=\"form-group text-center\">\n            <button class=\"btn btn-success\">ADD</button>\n        </div>\n    </form>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/payments/history/history.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/payments/history/history.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <table class=\"table\">\n        <thead>\n            <th>Vendorname</th>\n            <th>Address</th>\n            <th>Amount</th>\n            <th>Date</th>\n            <th>Account no</th>\n            <th>IFSC Code</th>\n            <th>Status</th>\n        </thead>\n        <tr *ngFor='let pay of clients'>\n            <td>{{pay.vendorname}}</td>\n            <td>{{pay.address1}}</td>\n            <td>{{pay.amount}}</td>\n            <td>{{pay.month}}</td>\n            <td>{{pay.acno}}</td>\n            <td>{{pay.code}}</td>\n            <td>{{pay.paystatus}}</td>\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/payments/nav/nav.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/payments/nav/nav.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav nav-expand-sm content-justify-center\">\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dashboard/addpayment\">Add payment</a>\n    </li>\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dashboard/viewpayment\">View payment</a>\n    </li>\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dashboard/viewpaymenthistory\">View payment history</a>\n    </li>\n</ul>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/payments/payments.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/payments/payments.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-nav></app-nav>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/payments/view/view.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/payments/view/view.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <table class=\"table\">\n        <thead>\n            <th>Account No.</th>\n            <th>IFSC Code</th>\n            <th>Googlepay No.</th>\n        </thead>\n        <tr *ngFor='let obj of payment'>\n            <td>{{obj.account}}</td>\n            <td>{{obj.ifsc}}</td>\n            <td>{{obj.pay}}</td>\n\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/profile/editprofile/editprofile.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/profile/editprofile/editprofile.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n\n        <div class=\"col-md-6\">\n            <h2 class=\"text-center text-warning\"><U>REGISTRATION FORM</U></h2>\n            <form #ref5=\"ngForm\" (ngSubmit)='edit(ref5.value)'>\n                <div class=\"form-group\">\n                    <label>ENTER NAME</label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" [(ngModel)]=\"currentuser.username\" readonly>\n                </div>\n                <div class=\"form-group\">\n                    <label>DATE OF BIRTH</label>\n                    <input type=\"date\" name=\"date\" class=\"form-control\" [(ngModel)]=\"currentuser.date\">\n                </div>\n                <div class=\"form-group\">\n                    <label>EMAIL ID</label>\n                    <input type=\"text\" name=\"email\" class=\"form-control\" [(ngModel)]=\"currentuser.email\">\n                </div>\n                <div class=\"form-group\">\n                    <label>MOBILE NO.</label>\n                    <input type=\"number\" name=\"number\" class=\"form-control\" [(ngModel)]=\"currentuser.number\"\n                        #ref1=\"ngModel\" minlength=\"10\" maxlength=\"10\">\n                    <label *ngIf=\"ref1.invalid\" class=\"text-danger\">*this field contain atleast 10 digits</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>PASSWORD</label>*\n                    <input type=\"password\" name=\"upassword\" class=\"form-control\" [(ngModel)]=\"currentuser.upassword\"\n                        #ref2=\"ngModel\" minlength=\"5\">\n                    <label *ngIf=\"ref2.invalid\" class=\"text-danger\">*this field contain atleast 5 characters</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>ADDRESS</label>\n                    <textarea name=\"address\" id=\"\" rows=\"5\" class=\"form-control\"\n                        [(ngModel)]=\"currentuser.address\"></textarea>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-info\" type=\"submit\">REGISTER</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/profile/profile.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/profile/profile.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navprofile></app-navprofile>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/profile/viewprofile/viewprofile.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/profile/viewprofile/viewprofile.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive\">\n    <h4 class=\"text-center\">VIEW PROFILE</h4>\n    <table class=\"table \">\n        <tr>\n            <td>Name:</td>\n            <td>{{currentuser.username}}</td>\n        </tr>\n        <tr>\n            <td>DOB:</td>\n            <td>{{currentuser.date}}</td>\n        </tr>\n        <tr>\n            <td>Email:</td>\n            <td>{{currentuser.email}}</td>\n        </tr>\n        <tr>\n            <td>Mobile No. :</td>\n            <td>{{currentuser.number}}</td>\n        </tr>\n        <tr>\n            <td>Address:</td>\n            <td>{{currentuser.address}}</td>\n        </tr>\n        <tr>\n            <td>Typeof user:</td>\n            <td>{{currentuser.user}}</td>\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/viewclient/viewclient.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/viewclient/viewclient.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <table class=\"table\">\n                <thead>\n                    <th>vendorname</th>\n                    <th>address</th>\n                    <th>email</th>\n                    <th>number</th>\n                    <th>eRent</th>\n                    <th>Status</th>\n                </thead>\n                <tr *ngFor='let view of clients'>\n                    <td>{{view.vendorname}}</td>\n                    <td>{{view.address}}</td>\n                    <td>{{view.email}}</td>\n                    <td>{{view.number}}</td>\n                    <td>{{view.eRent}}</td>\n                    <td>\n                        <div *ngIf='view !=undefined'>\n                            <button type=\"submit\" class=\"btn btn-success\" (click)='accept(view)'>Accept</button>\n                            <button type=\"submit\" class=\"btn btn-danger mt-1\" (click)='reject(view)'>Reject</button>\n                        </div>\n                    </td>\n                </tr>\n            </table>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/board/viewhouse/viewhouse.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/board/viewhouse/viewhouse.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <table class=\"table\">\n        <thead>\n            <th>HouseType</th>\n            <th>Address</th>\n            <th>Rent</th>\n            <th>Image</th>\n        </thead>\n        <tr *ngFor='let obj of house'>\n            <td>{{obj.house}}</td>\n            <td>{{obj.address}}</td>\n            <td>{{obj.rent}}</td>\n            <td>{{obj.image}}</td>\n            <button class=\"btn btn-danger\" (click)='delete(obj.address)'>delete</button>\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/carousel/carousel.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/carousel/carousel.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\n\n    <!-- Indicators -->\n    <ul class=\"carousel-indicators\">\n      <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n      <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n      <li data-target=\"#demo\" data-slide-to=\"2\"></li>\n    </ul>\n\n    <!-- The slideshow -->\n    <div class=\"carousel-inner\">\n      <div class=\"carousel-item active\">\n        <img src=\"https://cdn.pixabay.com/photo/2014/07/10/17/18/large-home-389271__340.jpg\"\n          style=\"width:100%\" height=\"500px\" alt=\"\">\n      </div>\n      <div class=\"carousel-item\">\n        <img src=\"https://pattersoncustomhomes.com/wp-content/uploads/2018/11/modern-beach-house-exterior-bay.jpg\" style=\"width:100%\"\n          height=\"500px\" alt=\"\">\n      </div>\n      <div class=\"carousel-item\">\n        <img src=\"https://images.squarespace-cdn.com/content/v1/53ff083fe4b06d598893dcdf/1416059184915-NCDOYJZRT6WO5NMZ18N5/ke17ZwdGBToddI8pDm48kFwLpwhcqxzfNuBZPTq0g3kUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYy7Mythp_T-mtop-vrsUOmeInPi9iDjx9w8K4ZfjXt2dnihgU9gKGLroRAsvpywHz61e7rLscY9kQ8yq3Ldx1LvP7cJNZlDXbgJNE9ef52e8w/Mirror+Houses+Slide+front.jpg\" style=\"width:100%\" height=\"500px\" alt=\"\">\n      </div>\n    </div>\n\n    <!-- Left and right controls -->\n    <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n      <span class=\"carousel-control-prev-icon\"></span>\n    </a>\n    <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n      <span class=\"carousel-control-next-icon\"></span>\n    </a>\n\n  </div>\n  <div class=\"card\">\n    <div class=\"card-header\">\n      <h3 class=\"text-primary\">About us:</h3>\n      <p>To get information about how rental houses are currently\n        being managed, I prepared questionnaires and submitted them to\n        a number of rental house managers and from the information I\n        gathered I realized all work was done manually with a lot of\n        paper work involved. Papers can easily get damaged or get lost\n        leading to loss of data. It is also expensive to keep on buying\n        files to store your records. A lot of files make a place look untidy\n        and also consume a lot of space. Getting a certain file to check\n        data from many files becomes a difficult task.\n      </p>\n    </div>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/changepassword/changepassword.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/changepassword/changepassword.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h2 class=\"text-center text-success\">Change password</h2>\n    <div class=\"row mt-3\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)='changepassword(ref.value)'>\n                <div class=\"form-group\">\n                    <label>username:</label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"\">Enter new password:</label>\n                    <input type=\"password\" name=\"password\" class=\"form-control\" ngModel>\n                </div>\n                <label>TYPE OF USER:</label>\n                <div class=\"form-check\">\n                    <label for=\"a\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"a\" value=\"owner\" class=\"form-check-input\" ngModel>Owner\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label for=\"b\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"b\" value=\"vendor\" class=\"form-check-input\" ngModel>Vendor\n                    </label></div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\">submit</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/contactus/contactus.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/contactus/contactus.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFRUVFRUVFxcWFxUXFRcQFRUWFhYVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lHx0tLS0rLSstLS0tKy0tLi0rLSsvLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgA/AMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAADBAIFBgEABwj/xAA+EAABAwIEAwUHAgUCBgMAAAABAAIDBBEFEiExBkFREyJhcZEUMkKBobHBUmIHI5LR8HLhFTRTgqLxJDNj/8QAGgEAAgMBAQAAAAAAAAAAAAAAAAECAwQFBv/EACsRAAICAgICAgICAAcBAAAAAAABAhEDIRIxBEETIlFhFJEVIzJxgaGxBf/aAAwDAQACEQMRAD8A+rCpPRTFT4IZY7ooOe4btVohxkwKJYHZINlafBGaehQFBtkvOwO2K695dpy+660AKEp+kSUShxPhl0pzCUAjkQbfdVUuAVEeuXMOrTf6bragqbXKPJkqZgoXEGxuE82Raetw6OUd4a8nDQrPV2FyRa+83qPyFJSQmjjHIock45EVrlMiMXUgUAOUw5ADDXIgclQVxxKVAOmQKDpwki1yj2bkwGJKoKTZbhIyRlMQjRJgdbunRayQe6ym6TRAwckIJUTTdEnJMQUVlWUAF7MhSaVD2m6jnQAw16IHJUOUg9MA5egSyKJel55NEgK6p7zwFdwU4DQqWgGaS/itE1oskNmkD1IOCRdPZD7YlTKxuofGBd1rJRkoJ00CzcmJdtIQD3Gmw8T1VxTg2WaeW3SNUcNJORaZlEpIVBbuvPrRlKhyLFjHwpByqIMSBTkdUCnyTB42ux0SKWa4slcwXWvspWQcSpxTCCLvi1HNo5eSqmSLXQPS9fg7JdR3XdRz8wpxkVOJnmvUw9DraKSE94afqGyE2VTsjQ616IJQkw5dCYqHe3C57W1KdmuimQAd9W1Gp3ghJGlVjSwgBAgDorlSkhUsQrY4I3SyGzGC5P2A8brM8N8YGsY92VjXNcQWl1u4fdOu/wDshbdA3SsfqWaoVkWWpvuI/wCon8Ib6q3/AE/RxUvjZH5UM0VDJJqxt7abga/MobiQSDuDY+YVhwzVB7wQRbXYEfQpKrkfnda/vO2aOp5qMYttr8Dc62Ra9SuehQxI+3vO9WhDLurvV4/Cs+Mj8v6CvekqufQolS7S4I03sb6dVUVk+iraplsXastcHG5VuGKswKM5bq5EQ6pAx1oJWb4ox4REQMdeR97/ALW21Ks8fxn2eGR4Gw08SvlmBOfUVRkdqXXJ8B0Uck6WieHHyds3/DtLoCtRDFoq7DIcoCto9llijXJtsTqrjdVNY/K1x6AlX1VFmHjuFmseNoXkcxb1IH5UZ6LsO3RW0NWTbVXNHUm/VZ2gbYKxpqlodboqYyo25Mdo1lPJdGedFS0+It6p+OfMLrQppnOyY2huOSyO2eyrQ4mwRWyaqakUcSz7RrhZwBB5FZnHMJMX8yPVnMfp/wBlcRvHkm2vBFiLg6KalRBxMRHOmWzhAx2h7CSw9x2rf7JNsitTKmi39oC77WqkuXMydipFp7VdPRS6XKoGP1VFxnxA7L7JCbvfo8jkD8I8eqGwoof4hcUGqk7GI/ymH+t/Nx8ByVZwnXGlma8khju6/Y9w87HodfkgNwd7XPvazHBpJIAzEXsLlTkgI0I1WGeZqVo7vj+DF43GXtH1xwda4L7HYgRgW6pSoa4jd3ze0fZU/BmICSLsn5c8VgLtJJj5em3otAY+g9Iz+V1YTU4po83lxPFNwl2it4Yc6kqHvcS6N42zAkPvv5EKylLXEnu6kn4juvBp/f8A0tC4b/u+bmj7JqKTIN2ca0dB8mH8orWeB/paEEkdR83k/ZdaR+3/AMipCGS24IN7EdWhZOuu15YeR+i1Ubx09Gf3VTxDQF+WRgNwQHaAd3roqskdWW4pU6LHCicgsrVkDrJfB4MrBdP9sFUWtmf/AIgUpdSOIPukOt1CzX8P6cEOe7W5s2+2m/8AngvoWIwh7HMOzgQstwph+UWt3WEtHiQdT63KozGrx32bGntYaWTkWySa6w1RWVACrui2mEfUA3HMLL8WSgR5er228ve/CsKqa0o6WPpoQPqViOKpZqmo7ON4YyPQk7l/O3lt6qmcuWjVihxaYKrxpsfcb3nnkNbeJt9l2gdM/wByNxJ5nut+d9U3hvD8sYu3I4+h+qsmumb70bh4gXH0VXGjX8n7C0GAuOskxv0boAfmtDR0rYxYOJ81TUs5Nt1cwNPzKnCvSMudyrbG6WO9z8kTJ3keNtgkzJqT4/ZaejD2MtjPRGYSOnkgtmCJFfdMiKcRUvawOsO8zvD5bj0WFimX05q+aYtR9lO9nLNcf6TqFZFlUiYlC6JEFgC5PKGNLtTYXsNSfAKZAUx7GBBHce+fdH5SHCeEEnt5dXu1F+QPPzKUoMNkqqgvla4Nb3gCCBvoFuaaHKFVORdjgfOOJKZ8kMwjYXE1OwFzZrTyUaFn86MdHMHpb+yLi9N2sNtf+Ykdp6bqeFt/+S3weT6XP4WOb+sV+z0OCFZckv0v/BTAsYlbUPkzOeGy5bX1yOLw5oPkB6BfT2vDmhwsQQCCZTqDqDosBhzS6WO7QCZhrkDSQATrYC62OHPMbnQ3NtXx2aD3Se835E38nLoePk+zief83BxhCXvd/wB6GXAdGernLwb/AKflGfyiEu//AE/8AguB53+clvsthzCYa7q75NaFxwPMn5uA+yFcfs/qc5dbblb5MP5QARpb1H9ZKLE9oPL0cUBt/wB3ya0fddLj1PzeB9kAWDXFxsNvwrCOnFkhhDwXZLi521ubrSR0OiokqZoUrRUzvA3/AMKHSxta3SwH5O6yuK47nls02DbgeJ5lGhxUncrBkzJs6uDxGo2+2X88xadNf7JaScj7qukrbhD9rvzVTnZqWOixr5v5ebmDp57W+qSog5je9Fn5nS518FH2m5awa63PkNAtTRFtgLKWNWV5ZcdGdkrWN90OYehBt9dlKHHo9idVrSweCXmghOj4mk+LQpOD/JX8qenEp6bEGvIyjMfAaq4pYC3vP94/QdESnMTPdaG+QCPmB3ThGvZTknekqREmw80jUnK032/zVHrJwBe+yr3ZpR0b9/JEn6FGPslSzl+vwjZPMlSVPDbQ7DYBNthv1CI3Q5VY/DKspx3TWcyYbHuHz3H5WlhAHMJPieDtKaQcwMw/7Tf+6uiyiSMCyRTLktGUW4UyFFvgLBaQ+DR9SnZTYHyKV4f9yT/U0fQote60bz0a77Kib2acS6R80Z2nee0kAE3sbeKFDM5rg5p1Gqdpv+XefH8BLULbyMH7m/cLBXR6q+79Ftg1c+aohD7dwuIsLfCVq8UgJAe0Xew5m8r6Wc246i49FR07G/8AEiGgANby0F+zbf6lak6rXhbVv9nB/wDocXKKS1xX/YswZwHBrLEXHvONj1CKykedgB5Rn8lX7v5cJfb3W3QsKqzK1ziAMpO3gLrd/I3Xs4fw6sqhQS9XejQq/F5DBku2SRz3ZQ1jtbgXT/B+NS1Rf2mWzbWsLbpjEKpjqiFjS0lnbvdY6gtZbXp7yr/kuUbXs1x8LhmcJq+Kbf8AV9mep5Kl7gPYXtBOrpHDQdbLSswba9r+AH5S2CNY6qnIe4uAa0gjut2Fgb67K8hae1ceVmgeiI5ZNXZHyMcFKlGtL8+/9ztPhcbLHvXFjvzVo2UKklr++4dCR6KPtxUHkvsuXj66PmLo9blMwy2VzinD8gJLBmHhv6KrjwadxsI3fMWHqVmlB9Ub4ZoNXZ41XK6BNXBn4V63hNzYnve+7g0kNbtprqeenRZqeHmUvia7EvJjJ1EssHk72Ym5Ov8Ast1QVDXAcvmsHhm9gthhlMCPeIPy+oTi9hlSas0Eev8AmqNbTWxCrnxPaMzTmtuOdvDqiU1cDz/zoreXpmVwdWg09G1212+S57COb3HysEXtQRZCjLr22Q0kR+35Iuom7Wv56qMhA02RpnFjS5xAA1JJsABzJ5L57V8Qz1sroqMFsY0Mx3I6t/SDy5nfZGvRFv8AJtH1FhplaOrnNaPVxCYjpy4aOJ8QyUj1DLLO0WGPYBlmLX85LAyHwDnbBWsGFNPvSzOPUyH7CwT0JKXfQ5M9sbmse7vOvlGoJI3sCASiGUOBbfcEeoVHxBh4jjzmRzmNc3uvNy1xcA17HDUEEj/NFaRTdpG1xPetbMNLkaXPmnY+P5PnzospI6Ej0KPDSPcC5o0aLlx90AcyTojywZ6hzdhmufAblaCevbFFlYwFtrWOxHj1U3NIgsUmVMGKMZHlfLGST8LQ0WtztuhYpWtMEhDge47n4Kjr8VoZMzH0wik5OZ3bn5f2VD/wioIJuAw7A3uW+NlXKmX44yi1S6Eg9SimLSHNNiDceaFVUT49S02H6Tf6FJMqQdneoIWf4X6Ov/iEOpaNlwpM6Spe9xu4sJJ8SQPwtox2oXzThrFWwvc52xaALEHnda+jx6Jzh3ra89PuroQaRzPLzRnktdUbTH6KWajkigNpHNaGm9rd5pJufAFB4bwmaGlMUrwZS14LhqMzgQDy20TZxaFoF5W3sNAbn0CWl4igb8R/pP5Wnj7OXyfQDhPhx1ExwfI2QuIN2gtGgtsSVWtoewq3OJBzRTSbbZnsaAeqcm4xpxzJ+bf7rO1vE7DVB7hZrohGL5rXz5r5gLchoq5Yo0v0a8fk5Lm2/wDWt/s0vD85fJUEyROAkLR2ZYS2xOjy0b+avmOuQsJhdfTU5e2JzQ6WQvIaXPJkcST73mtRT0lVIM7HBg5CQXJ8bNOymo0iibc5XRnn1DmTyMfuHE/Im4TjWSP1YNEHGqXLKwzSNc+xuGty3byvqfFP02JgNAFgFienR2FJuKaRcGNRLU92ag+JdE4Iu0DmsHi+HBj3tAtYn05LfOhVPjGHFwzAXPPrl/2VeRWi7DLjIxdCSCtNhVSOqzMzcpI8U1h0+uqwyVM60WpRo+g0s1woYhQ5hnj0eNbcneB8fFK4c/QWVvErkrRkb4y0UNFJK/UNNtrnQXG4ueavGNOht9Rv0HVV3EFYIWOmOzWG3gRc+mt/kVHCq1szA4G7mtAc3pYbj9p6+KsjFfkqnlk+lpGe4vxxss4oh7pyB/jI8gNZp0uPmRtutdhmAtgi7NrBtraxN+f/ALWMj4MjrXPfO5zXOkc7NHlDzbRuYlpzffQarecMYQaWBkHbSzBl7PlIL8pNw3QbDkpKCKZZXr9GeqZXRvLHaEbXFrt5FddieQC/NbWroo5RaRod06jyO4Xzv+IdBLSxiaNpdE2+Z/8A0wbWLwNSPEfOyreOUejXj8iE9S0KcTYqZmsgb8bwXHwab/55K6o5wIw3z9SVjeCaW8nbyRuLyTlMrtmkDZg2257L6XYEbCyrSb9l0nFUqMRi1T2Up/cAfTRJyYlmBCvuJcLDgHbAaHwvsfL+6oKbDw33nA9LXJ/2UHGd6NUM2FQ+2mhHBsBEs4lk9yM3H7n8vkN/Rbx2HMcNAstNVmPQCzeXzRqXHi0apxnWmKeJy+0PZHH8IaGk21VLgvAbXHtJxe5uGbAD91tz4LZUMxqLSO0b8I6/uV5DAOSnHfRRlaTqXaMyzhCntbsWW/0hVWL/AMP+6X0rzG8fDc5T4eC+htbZIVdZc5Wb8ypVxKbcnR8TrJ8QdKKYFxkJtYdOpJ2Hit7w3wE0NDquR0zzqW5iIx4WHvfNa6gw1rSXBozHd1hc+ZVo0AKdtlTjFdFbRYDTR+5Cxvk0X9U4/CY3iz2NcOhAsmjoFHt0Ed+igxHhSl3bEGn9otr4KNHiToRlJJDevRX0puFncXoS6+Xcquba2i/Dxb4zMhWVj5pXyH4jp4N5BWlBQSvZma0kXTmE4U29pBa31WnhqMoDRGbDQWIssyxyltm7JnjBcYIuMi4Y0wLKWi6x50TMKDNCrOwVXjFUGNSaHZguJaDLKXDY3d/dU9I4B2qvq2dzr5gSCs7UxlhWTNCtnR8bJembbB5gRZX0TlgMFqzcarY0k9wFHHInlhuxnFqdr4ngtzWBIGxJAOg8V8moZjA8PpJc8W4Ye7LEDyYHd2Rn7ASeg5L6/HIviHG3DNRS1czoCRC89qwbtAfq5tjoLOzfKys7Ka2fU+FOIYZLNc5rHO217jr/AKSdj+06+a3MQC/P3AmAy10Ujm1HZyNflewtDo8hHdeWnWxcDexGxUqqTFaGq9jEjwTlDXRyPEZdICWBrXXAuWkeeisjdbM2RRv6n6CVTxNiIhgcQMznDK1u9ydNR0818u4Y45rqeripq7M9spy3ka0FhOjXB7QLjNYG99CvolXROJ7eVrnhpBDWa/y/iyt520NuevgpWV1RjpadkckYa/K5wu+P9B0tboDrp4LVUr+6FnMY4aETvaaU9pA85iQ4uc0ncknUt8eXNW+EyXaFn6kzo3cE7ug9fEXNcNwQR9FioagHdb8hfPsei7OokDdBcO/qAP3urIOnRTl2rDSMa4WO31B6hDOGtDblwIHKxuUnHUW3VhHMCNdijJiUt+x4fJnj1egsOLWNhsOnRXdBi11lZKIg35HY/wCc1a4ZSk6nRo3J0CyJyWjqT+Jx5ei/xLFA2Jzm72081nMPxXa6JWV5JswWA58yPwqeSjc11wO6drbeXgpZFONNlPjzxSuKNxSVwIGqf7cELMYfGQ27iABuToFYDEYWbvzHo3X1OynCUmVZowi+y2a4ld23Ko5MVe490taPUqQqn9b+JU9lH/KLsP0QmalUFbj8UVs7iT0aCfqoxcYU5+MDzTItF9UMHRC7J/IpBvEdOde0b6rp4rph8bUqTGpNFoK1/VTbWP6pMFFYVqMA42rf1XHtze8oMCM1AAvZWdAqTifBA5naMGo0cPDqtCiAcjsdPklKPJUSjNxdo+UxPMbtVqMMrS61hfySfFeEdkbj3Tct8P2/JJ4LVZdFgkuDOxjayRN3C+4BS+MUQmjs4bfbmP8AOiWoqnRWUbrhWp2Uyg4s+Q4zhVbQmSSma7K4HvRuc1zRcE7b7bLRcL8RR4tC2Op7tREMrpBZuYl5MYDbDvNsHafpO11uBbVpH/pfNOOOFfZ5BW0wtY3kjGgcOYI2IKlF0QnDm6NjxpgD6mLPTjLUx95p6yM5eGYdOqwmB/xXqaWLI5gks4Ds5A4OZY98NcNhysdifCy+p8JYv2sccpIOYBr7XsDyvcaEaXVTxXwLTuqHTCMWmJLvCX4j89/O6sb1ZmhH7cWKYBxrTTva+jd2c8tzJRSXDZCLk9lJbLnsCdDrzG60FO6F4MkIy6kPYRZzH82lvLy9F8rx7gt9M4SxAlgN7tJDmnqCNR5hAwDi90BPbyPcAQGynvPazYsmG80fzzN5X2UeSei14pQXKJ9fc5Go6Fkjczmgm5HpZUtHWF2ltcodlvfuHUPjd8bD15c1o8IN2G36j9gnBVIjlmpY9dkP+DQ/oHoFIYRF+keieXCVcY7YmMMjGwHovPw9h3Cac5Be9FDtijsLi/SEI0EY208tEzJIk5ZkDF6qgY73rnzN1Xy4fGNgm5Zks+RA9irqcDZGazKFB0iFNVFBIDXRh27QVnMQwRknwWPgryWocgF0pUHsadGcbwVI/wB0keZVjTfwyaW3fOQ7wVxDHN+shMtw2Q6l7vUpqISmy7CIxcsutUioYYUdpSrUZhSAMptKGFIJgRr6Ns0bo3c9j0dyK+ZzMdDIWkWINj5jRfUmqg4qwITNMjB3wNQPiA5+YVObHyWjV4ub45U+mUuGVN+a0VFKsBR1BYbLT4fXAjdZYSp0dXLHkrReVrSLPby3/wBPNDrG9pG4aG4KlBUgix5pWnkDXOb0P05K2zKov+jL8N8V+zuFBUxlgYTkdYC0Z3294X73W/XZfThVRuhcJXtbkGriRa3wvv0P1WXxnA4KyMslbr8Lxo9juTmu5FfP6almp5+wq3mURkOYT7r2a5HeNtRY7G6fPitkf46yz+umfUGPbI21rghfL+JuGhBUnK3+XJ3m9Afib66/NfQsPxRhA5KHEULZob2uWEOH2P0KrluOjTi/y501oy/DbmxNMb45XMc8ObJG67qZ+UhzmsOuU2bfL6Fa3CcVIeG5muLnZQ5ukcjh8Jaf/rk/ad+VxvWYFh7ie6PwPVXj+GKfvEZml4aHhjrMflOYXYbga822IueqtwOTWzH50McZ/X36NBm6gjwO4US5BdIhulWk54VzkF70N0yXkmQFE5XpOV69JMlXvulZJIjI9LP1RyFwMUSYsIlF9On2xojYEAVIpSmoaJWcdME5FAgTYjT0SfZSpqKNNBikQZRZV7KpkLiYHmojVAKQKADNKkEIFTBSEGCmCgBylmTAznE3DrXAzRCzt3NHMdWjr4LJQPLTuvqAkWdxbhtr3F8RDSd28ieoWXNhvcToeL5fH6z6KmjriFGmxQOmeb6XA9BZWFPwy4gh7w0WNra68vks9X4W+B3eFvEe6fEFUOM0rZuhmxTk0mbamnBsQUjxThPtEWZg/mR3c08yPiZ87eoCoMOxFzVpcOr7qampKhPHKD5R9GOwqV2lltsHJNg7UeKCzAomuLmki5vbS2uuisI8rBoFPHhknsp8nzITjUex/tANgB5IT50o+ZLvnWo5NDrp0F86SdKoF6LJUNvnQHzIRcokqNjok565mQy5ezIJUFCIEBpRQUh0GaUdhSzEdiBNDUZTUZSbExGVIgxxhRw5LMRQmIqCVwlQLlEuTEEzLwcgly9mSAYD1IPSoeu50BQ12i4ZEvnUHSIHQczKPtCSklQHTIsdFp7SlMUjEzC077jzSnbrnbKLpqmSjadoz3sxY7KRqPqOqvcMhKmXg7gHzU+0/wACzLBs6D8241Wx2Sp6ITp0vddDVpOe9kzIoF69lXsiAOZl0FeDFIMQBFcIRmxoghQFihYutjTradHZTooOQi2FGbTp9kCM2FFC5CDIEdkKdbEiNjToVirIkdkaOI1MRoEDa1FAUw1SypgZklDc5cXkwI5lzOvLyQzmdd7ReXkgo8Xobnry8ixgJHJd5Xl5Ikgd1ILy8kMK1qNHGvLyaIsYZAitpl5eUqIkvZl32Yry8ihWdFKptpl5eRQWEbT+CI2BeXkUKwjYUVsS8vJgFbGiNYvLyAsmGogauLyQiYapALy8gDoC7ZeXkAf/2Q==\"\n                width=\"100%\" height=\"500px\" alt=\"\">\n        </div>\n        <div class=\"col-md-6\">\n            <h4 class=\"float-right\">\n                <ul>CONTACT US:-</ul>\n                <p>E-mail:</p>\n                <p>support12@gmail.com</p>\n                <p>Mobile no.:</p>\n                <p>9548854555</p>\n                <p>whatsupNo.:</p>\n                <p>9548854555</p>\n            </h4>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/forgotpassword/forgotpassword.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/forgotpassword/forgotpassword.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)='password(ref.value)'>\n                <div class=\"form-group mt-3\">\n                    <label>EnterUserid:</label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" ngModel>\n                </div>\n                <label>TYPE OF USER:</label>\n                <div class=\"form-check\">\n                    <label for=\"k\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"k\" value=\"owner\" class=\"form-check-input\" ngModel>Owner\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label for=\"i\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"i\" value=\"vendor\" class=\"form-check-input\" ngModel>Vendor\n                    </label>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\">submit</button>\n                </div>\n            </form>\n        </div>\n        <div class=\"col-md-3\"></div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <h2 class=\"text-center text-primary\">LOGIN FORM FOR OWNER/VENDOR</h2>\n            <form #ref=\"ngForm\" (ngSubmit)='method(ref.value)'>\n                <div class=\"form-group\">\n                    <label>UserName</label>*\n                    <input type=\"text\" name=\"name\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                    <label *ngIf=\"ref1.invalid && ref1.touched\">\n                        <div class=\"text-danger\">*this field is mandatory</div>\n                    </label>\n                </div>\n                <div class=\"form-group\">\n                    <label>Password</label>*\n                    <input type=\"password\" name=\"password\" class=\"form-control\" ngModel #ref2=\"ngModel\" minlength=\"5\">\n                    <label *ngIf=\"ref2.invalid \" class=\"text-danger\">*this field contain atleast 5 characters</label>\n                </div>\n                <label>TYPE OF USER:</label>*\n                <div><label *ngIf=\"ref3.invalid\" class=\"text-danger\">*user is mandatory</label></div>\n                <div class=\"form-check\">\n                    <label for=\"b\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"b\" value=\"owner\" class=\"form-check-input\" ngModel\n                            #ref3=\"ngModel\" required>Owner\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label for=\"n\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"n\" value=\"vendor\" class=\"form-check-input\" ngModel\n                            #ref3=\"ngModel\" required>Vendor\n                    </label>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\" type=\"submit\">LOGIN</button>\n                </div>\n                <div class=\"text-center\">\n                    <a routerLink=\"/nav/forgotpassword\">Forgotpassword?</a></div>\n                <!--\n        <h5 class=\"text-center text-danger\" *ngIf=\"login\">{{login}}</h5>-->\n            </form>\n            <div class=\"form-group text-center\">\n                New User?<a routerLink=\"/nav/register\"><u>Click here to Register</u></a>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"col-md-3\"></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/nav.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <h2 class=\"text-dark\">\n    <marquee behavior=\"alternate\" direction=\"left-right\">HOUSE RENTAL MANAGEMENT SYSTEM</marquee>\n  </h2>\n  <nav class=\"navbar navbar-expand-md bg-dark navbar-dark\">\n    <!-- Brand -->\n    <a class=\"navbar-brand\" href=\"#\">\n      <img src=\"https://i7.pngguru.com/preview/26/980/998/house-plan-building-latur-villa-house-vector.jpg\" width=\"100px\"\n        height=\"60px\" alt=\"\">\n    </a>\n\n    <!-- Toggler/collapsibe Button -->\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#C\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n\n    <!-- Navbar links -->\n    <div class=\"collapse navbar-collapse\" id=\"C\">\n      <ul class=\"navbar-nav\">\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"carousel\">HOME</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"/admin/login\">ADMIN</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"login\">LOGIN</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"contactus\">CONTACT US</a>\n        </li>\n      </ul>\n    </div>\n  </nav>\n</div>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/otp/otp.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/otp/otp.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h2 class=\"text-center text-warning\">OTP Verification</h2>\n    <div class=\"row mt-3\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)='otp(ref.value)'>\n                <div class=\"form-group\">\n                    <label for=\"\">Enter OTP:</label>\n                    <input type=\"number\" name=\"OTP\" class=\"form-control\" ngModel>\n                    <P>OTP expires in 1 minute</P>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\">submit</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/register/register.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/register/register.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <h2 class=\"text-center text-warning\"><U>REGISTRATION FORM</U></h2>\n            <form #ref=\"ngForm\" (ngSubmit)='Change(ref.value)'>\n                <div class=\"form-group\">\n                    <label>ENTER NAME</label>*\n                    <input type=\"text\" name=\"username\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                    <label *ngIf=\"ref1.invalid && ref1.touched\">\n                        <div class=\"text-danger\">*this field is mandatory</div>\n                    </label>\n                </div>\n                <div class=\"form-group\">\n                    <label>DATE OF BIRTH</label>\n                    <input type=\"date\" name=\"date\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label>EMAIL ID</label>\n                    <input type=\"text\" name=\"email\" class=\"form-control\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"mobile\">MOBILE NO.</label>\n                    <input type=\"tel\" name=\"number\" id=\"mobile\" class=\"form-control\" ngModel #ref2=\"ngModel\"\n                        minlength='10' maxlength='10'>\n                    <label *ngIf=\"ref2.invalid\" class=\"text-danger\">*this field contain atleast 10 digits</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>PASSWORD</label>*\n                    <input type=\"password\" name=\"upassword\" class=\"form-control\" ngModel #ref3=\"ngModel\" minlength=\"5\">\n                    <label *ngIf=\"ref3.invalid\" class=\"text-danger\">*this field contain atleast 5 characters</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>ADDRESS</label>\n                    <textarea name=\"address\" id=\"\" rows=\"5\" class=\"form-control\" ngModel></textarea>\n                </div>\n                <label>TYPE OF USER:</label>*\n                <div><label *ngIf=\"ref4.invalid\" class=\"text-danger\">*user is mandatory</label></div>\n                <div class=\"form-check\">\n                    <label for=\"c\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"c\" value=\"owner\" class=\"form-check-input\" ngModel\n                            #ref4=\"ngModel\" required>Owner\n                    </label>\n                </div>\n                <div class=\"form-check\">\n                    <label for=\"h\" class=\"form-check-label\">\n                        <input type=\"radio\" name=\"user\" id=\"h\" value=\"vendor\" class=\"form-check-input\" ngModel\n                            #ref4=\"ngModel\" required>Vendor\n                    </label>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-success\" type=\"submit\">REGISTER</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>\n<div class=\"col-md-3\"></div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/home/home.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/home/home.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h3 class=\"text-warning\">VENDOR DASHBOARD</h3>\n    <div class=\"row\">\n        <div class=\"col-md-3\">\n            <div class=\"card text-center\">\n                <div class=\"form-control\">\n                    <a routerLink=\"/home/profile\">Profile</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"whoomtolet\">Whom to let</a>\n                </div>\n                <div class=\"form-control\">\n                    <a routerLink=\"payments\">Payments</a>\n                </div>\n                <div class=\"\">\n                    <button class=\"btn text-primary\" (click)='logout()'>Logout</button>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <router-outlet></router-outlet>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/dopay/dopay.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/payments/dopay/dopay.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h2 class=\"text-center text-info\"><u>MAKE YOUR PAYMENT</u></h2>\n    <form #ref4=\"ngForm\" (ngSubmit)='dopay(ref4.value)'>\n        <div class=\"form-group\">\n            <label>owner name:</label>\n            <input type=\"text\" name=\"owner\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n            <label *ngIf=\"ref1.invalid && ref1.touched\" class=\"text-danger\">*this field are mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>Address:</label>\n            <textarea type=\"text\" name=\"address1\" rows=\"5\" id=\"\" class=\"form-control\" ngModel #ref2=\"ngModel\"\n                required></textarea>\n            <label *ngIf=\"ref2.invalid && ref2.touched\" class=\"text-danger\">*this field are mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>Date:</label>\n            <input type=\"date\" name=\"month\" class=\"form-control\" ngModel #ref3=\"ngModel\" required>\n            <label *ngIf=\"ref3.invalid && ref3.touched\" class=\"text-danger\">*this field are mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>Amount:</label>\n            <input type=\"number\" name=\"amount\" class=\"form-control\" ngModel #ref5=\"ngModel\" required>\n            <label *ngIf=\"ref5.invalid && ref5.touched\" class=\"text-danger\">*this field are mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>Account Number:</label>\n            <input type=\"text\" name=\"acno\" class=\"form-control\" ngModel #ref6=\"ngModel\" required>\n            <label *ngIf=\"ref6.invalid && ref6.touched\" class=\"text-danger\">*this field are mandatory</label>\n        </div>\n        <div class=\"form-group\">\n            <label>IFSC Code:</label>\n            <input type=\"type\" name=\"code\" class=\"form-control\" ngModel #ref7=\"ngModel\" required>\n        </div>\n        <div class=\"form-group text-center\">\n            <button class=\"btn btn-success\">PAY</button>\n        </div>\n    </form>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/nav/nav.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/payments/nav/nav.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav nav-expand-sm content-justify-center\">\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/home/dopay\">Do payment</a>\n    </li>\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/home/paymenthistory\">View payment history</a>\n    </li>\n</ul>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/paymenthistory/paymenthistory.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/payments/paymenthistory/paymenthistory.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h2 class=\"text-center text-primary\"><u>PAYMENT HISTORY</u></h2>\n    <table class=\"table\">\n        <thead>\n            <th>Owner name</th>\n            <th>Address</th>\n            <th>Date</th>\n            <th>Amount</th>\n            <th>Accountno</th>\n            <th>IFSC code</th>\n        </thead>\n        <tr *ngFor='let pay of viewhistory'>\n            <td>{{pay.owner}}</td>\n            <td>{{pay.address1}}</td>\n            <td>{{pay.month}}</td>\n            <td>{{pay.amount}}</td>\n            <td>{{pay.acno}}</td>\n            <td>{{pay.code}}</td>\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/payments.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/payments/payments.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-nav></app-nav>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/edit/edit.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/profile/edit/edit.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n\n        <div class=\"col-md-6\">\n            <h2 class=\"text-center text-warning\"><U>EDIT PROFILE</U></h2>\n            <form #ref6=\"ngForm\" (ngSubmit)='edit1(ref6.value)'>\n                <div class=\"form-group\">\n                    <label>ENTER NAME</label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" [(ngModel)]=\"currentuser.username\">\n                </div>\n                <div class=\"form-group\">\n                    <label>DATE OF BIRTH</label>\n                    <input type=\"date\" name=\"date\" class=\"form-control\" [(ngModel)]=\"currentuser.date\">\n                </div>\n                <div class=\"form-group\">\n                    <label>EMAIL ID</label>\n                    <input type=\"text\" name=\"email\" class=\"form-control\" [(ngModel)]=\"currentuser.email\">\n                </div>\n                <div class=\"form-group\">\n                    <label>MOBILE NO.</label>*\n                    <input type=\"number\" name=\"number\" class=\"form-control\" [(ngModel)]=\"currentuser.number\"\n                        #ref1=\"ngModel\" minlength=\"10\" maxlength=\"10\">\n                    <label *ngIf=\"ref1.invalid\" class=\"text-danger\">*this field is atlast 10 digits</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>PASSWORD</label>*\n                    <input type=\"password\" name=\"upassword\" class=\"form-control\" ngModel #ref2=\"ngModel\" minlength=\"5\"\n                        required>\n                    <label *ngIf=\"ref2.invalid\" class=\"text-danger\">*this field contain atleast 5 characters</label>\n                </div>\n                <div class=\"form-group\">\n                    <label>ADDRESS</label>\n                    <textarea name=\"address\" id=\"\" rows=\"5\" class=\"form-control\"\n                        [(ngModel)]=\"currentuser.address\"></textarea>\n                </div>\n                <div class=\"form-group text-center\">\n                    <button class=\"btn btn-info\" type=\"submit\">REGISTER</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/profile.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/profile/profile.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav nav-expand-sm content-justify-center\">\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/home/viprofile\">View profile</a>\n    </li>\n    <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/home/edit\">Edit profile</a>\n    </li>\n</ul>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/viprofile/viprofile.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/profile/viprofile/viprofile.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive\">\n    <h4 class=\"text-center\">VIEW PROFILE</h4>\n    <table class=\"table\">\n        <tr>\n            <td>Name:</td>\n            <td>{{currentuser.username}}</td>\n        </tr>\n        <tr>\n            <td>DOB:</td>\n            <td>{{currentuser.date}}</td>\n        </tr>\n        <tr>\n            <td>Email:</td>\n            <td>{{currentuser.email}}</td>\n        </tr>\n        <tr>\n            <td>Mobile No. :</td>\n            <td>{{currentuser.number}}</td>\n        </tr>\n        <tr>\n            <td>Address:</td>\n            <td>{{currentuser.address}}</td>\n        </tr>\n        <tr>\n            <td>Typeof user:</td>\n            <td>{{currentuser.user}}</td>\n        </tr>\n    </table>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tenanet/whoomtolet/whoomtolet.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tenanet/whoomtolet/whoomtolet.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <input type=\"text\" name=\"search\" id=\"\" placeholder=\"search\" [(ngModel)]=\"searchTerm\">\n</div>\n<table class=\"table\">\n    <thead>\n        <th>House type</th>\n        <th>Address</th>\n        <th>Rent</th>\n        <th>Rules</th>\n        <th>Image</th>\n        <th>Expected Rate</th>\n        <th>Status</th>\n    </thead>\n    <tr *ngFor=\"let obj of house | search : searchTerm\">\n        <td>{{obj.house}}</td>\n        <td>{{obj.address}}</td>\n        <td>{{obj.rent}}</td>\n        <td>{{obj.rules}}</td>\n        <td>{{obj.image}}</td>\n        <td><input type=\"text\" name=\"expectedrent\" class=\"form-control\" #ref9></td>\n        <td><button class=\"btn btn-info\"\n                *ngIf=\"!(currentuser.username==obj.vendorname)&&(obj.reqstatus !='request accepted')\"\n                (click)='Changestatus(ref9.value,obj)'>click for request</button>\n            <!--<button class=\"btn btn-danger\" *ngIf=\"check\" (click)='requestdata()'>check request</button>-->\n            <p *ngIf='currentuser.username==obj.vendorname'>{{obj.reqstatus}}</p>\n            <p *ngIf=\"!(currentuser.username==obj.vendorname)&&(obj.reqstatus =='request accepted')\">house booked</p>\n        </td>\n    </tr>\n</table>"

/***/ }),

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin/admin.component.ts");
/* harmony import */ var _admin_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/login/login.component */ "./src/app/admin/admin/login/login.component.ts");
/* harmony import */ var _admin_viewprofiles_viewprofiles_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin/viewprofiles/viewprofiles.component */ "./src/app/admin/admin/viewprofiles/viewprofiles.component.ts");






const routes = [{
        path: 'admin', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_3__["AdminComponent"], children: [{
                path: 'login',
                component: _admin_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
            },
            {
                path: 'viewprofiles',
                component: _admin_viewprofiles_viewprofiles_component__WEBPACK_IMPORTED_MODULE_5__["ViewprofilesComponent"]
            },
        ]
    }];
let AdminRoutingModule = class AdminRoutingModule {
};
AdminRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AdminRoutingModule);



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin/admin.component.ts");
/* harmony import */ var _admin_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin/login/login.component */ "./src/app/admin/admin/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _admin_viewprofiles_viewprofiles_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./admin/viewprofiles/viewprofiles.component */ "./src/app/admin/admin/viewprofiles/viewprofiles.component.ts");








let AdminModule = class AdminModule {
};
AdminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_admin_admin_component__WEBPACK_IMPORTED_MODULE_4__["AdminComponent"], _admin_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"], _admin_viewprofiles_viewprofiles_component__WEBPACK_IMPORTED_MODULE_7__["ViewprofilesComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _admin_routing_module__WEBPACK_IMPORTED_MODULE_3__["AdminRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]
        ]
    })
], AdminModule);



/***/ }),

/***/ "./src/app/admin/admin/admin.component.css":
/*!*************************************************!*\
  !*** ./src/app/admin/admin/admin.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluL2FkbWluLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/admin/admin.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/admin/admin.component.ts ***!
  \************************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AdminComponent = class AdminComponent {
    constructor() { }
    ngOnInit() {
    }
};
AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-admin',
        template: __webpack_require__(/*! raw-loader!./admin.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/admin/admin.component.html"),
        styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin/admin.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AdminComponent);



/***/ }),

/***/ "./src/app/admin/admin/login/login.component.css":
/*!*******************************************************!*\
  !*** ./src/app/admin/admin/login/login.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/admin/login/login.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/admin/login/login.component.ts ***!
  \******************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let LoginComponent = class LoginComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
    }
    submit(data) {
        this.http.post('admin/login', data).subscribe(res => {
            alert(res["message"]);
            if (res["message"] == "admin login successfully..")
                this.router.navigate(['admin/viewprofiles']);
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/admin/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/admin/admin/login/login.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/admin/admin/viewprofiles/viewprofiles.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/admin/admin/viewprofiles/viewprofiles.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluL3ZpZXdwcm9maWxlcy92aWV3cHJvZmlsZXMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/admin/viewprofiles/viewprofiles.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/admin/admin/viewprofiles/viewprofiles.component.ts ***!
  \********************************************************************/
/*! exports provided: ViewprofilesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewprofilesComponent", function() { return ViewprofilesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ViewprofilesComponent = class ViewprofilesComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
        this.http.get('admin/viewprofiles').subscribe(res => {
            console.log(res["data"]);
            console.log(res["data1"]);
            this.data = res['data'];
            this.data1 = res['data1'];
        });
    }
    logout() {
        this.router.navigate(['/nav/carousel']);
    }
};
ViewprofilesComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ViewprofilesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewprofiles',
        template: __webpack_require__(/*! raw-loader!./viewprofiles.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/admin/viewprofiles/viewprofiles.component.html"),
        styles: [__webpack_require__(/*! ./viewprofiles.component.css */ "./src/app/admin/admin/viewprofiles/viewprofiles.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ViewprofilesComponent);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/carousel/carousel.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./forgotpassword/forgotpassword.component */ "./src/app/forgotpassword/forgotpassword.component.ts");
/* harmony import */ var _otp_otp_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./otp/otp.component */ "./src/app/otp/otp.component.ts");
/* harmony import */ var _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./changepassword/changepassword.component */ "./src/app/changepassword/changepassword.component.ts");
/* harmony import */ var _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./contactus/contactus.component */ "./src/app/contactus/contactus.component.ts");











const routes = [
    {
        path: '',
        redirectTo: 'nav/carousel',
        pathMatch: 'full'
    },
    {
        path: 'nav',
        component: _nav_nav_component__WEBPACK_IMPORTED_MODULE_6__["NavComponent"],
        children: [{
                path: 'carousel',
                component: _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__["CarouselComponent"]
            },
            {
                path: 'login',
                component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
            },
            {
                path: 'register',
                component: _register_register_component__WEBPACK_IMPORTED_MODULE_5__["RegisterComponent"]
            },
            {
                path: 'forgotpassword',
                component: _forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_7__["ForgotpasswordComponent"]
            },
            {
                path: 'otp',
                component: _otp_otp_component__WEBPACK_IMPORTED_MODULE_8__["OtpComponent"]
            },
            {
                path: 'changepassword',
                component: _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_9__["ChangepasswordComponent"]
            },
            {
                path: 'contactus',
                component: _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_10__["ContactusComponent"]
            },
            { path: 'admin', loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./admin/admin.module */ "./src/app/admin/admin.module.ts")) },
            { path: 'board', loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./board/board.module */ "./src/app/board/board.module.ts")) },
            { path: 'tenanet', loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./tenanet/tenanet.module */ "./src/app/tenanet/tenanet.module.ts")) }
        ]
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'project1';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/carousel/carousel.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _board_board_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./board/board.module */ "./src/app/board/board.module.ts");
/* harmony import */ var _tenanet_tenanet_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./tenanet/tenanet.module */ "./src/app/tenanet/tenanet.module.ts");
/* harmony import */ var _admin_admin_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./admin/admin.module */ "./src/app/admin/admin.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _authorization_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./authorization.service */ "./src/app/authorization.service.ts");
/* harmony import */ var _forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./forgotpassword/forgotpassword.component */ "./src/app/forgotpassword/forgotpassword.component.ts");
/* harmony import */ var _otp_otp_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./otp/otp.component */ "./src/app/otp/otp.component.ts");
/* harmony import */ var _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./changepassword/changepassword.component */ "./src/app/changepassword/changepassword.component.ts");
/* harmony import */ var _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./contactus/contactus.component */ "./src/app/contactus/contactus.component.ts");



















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselComponent"],
            _nav_nav_component__WEBPACK_IMPORTED_MODULE_6__["NavComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
            _register_register_component__WEBPACK_IMPORTED_MODULE_13__["RegisterComponent"],
            _forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_15__["ForgotpasswordComponent"],
            _otp_otp_component__WEBPACK_IMPORTED_MODULE_16__["OtpComponent"],
            _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_17__["ChangepasswordComponent"],
            _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_18__["ContactusComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
            _board_board_module__WEBPACK_IMPORTED_MODULE_8__["BoardModule"],
            _tenanet_tenanet_module__WEBPACK_IMPORTED_MODULE_9__["TenanetModule"],
            _admin_admin_module__WEBPACK_IMPORTED_MODULE_10__["AdminModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
        ],
        providers: [{
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HTTP_INTERCEPTORS"],
                useClass: _authorization_service__WEBPACK_IMPORTED_MODULE_14__["AuthorizationService"],
                multi: true
            }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/authorization.service.ts":
/*!******************************************!*\
  !*** ./src/app/authorization.service.ts ***!
  \******************************************/
/*! exports provided: AuthorizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizationService", function() { return AuthorizationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AuthorizationService = class AuthorizationService {
    constructor() { }
    intercept(req, next) {
        //read token from local storage
        const idToken = localStorage.getItem("idToken");
        //if token is found,addd it tohandler of req object
        if (idToken) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + idToken)
            });
            //if token is not found,forward the same req object
            return next.handle(cloned);
        }
        else {
            return next.handle(req);
        }
    }
};
AuthorizationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AuthorizationService);



/***/ }),

/***/ "./src/app/board/addhouse/addhouse.component.css":
/*!*******************************************************!*\
  !*** ./src/app/board/addhouse/addhouse.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL2FkZGhvdXNlL2FkZGhvdXNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/board/addhouse/addhouse.component.ts":
/*!******************************************************!*\
  !*** ./src/app/board/addhouse/addhouse.component.ts ***!
  \******************************************************/
/*! exports provided: AddhouseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddhouseComponent", function() { return AddhouseComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let AddhouseComponent = class AddhouseComponent {
    constructor(http, hc) {
        this.http = http;
        this.hc = hc;
    }
    ngOnInit() {
    }
    addhouse(data) {
        if (data.address == "") {
            alert("address field is mandatory");
        }
        else {
            //console.log(data)
            data.username = this.hc.currentuser[0].username;
            this.http.post('/dashboard/addhouse', data).subscribe((res) => {
                alert(res["message"]);
            });
        }
    }
};
AddhouseComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
AddhouseComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-addhouse',
        template: __webpack_require__(/*! raw-loader!./addhouse.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/addhouse/addhouse.component.html"),
        styles: [__webpack_require__(/*! ./addhouse.component.css */ "./src/app/board/addhouse/addhouse.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], AddhouseComponent);



/***/ }),

/***/ "./src/app/board/board-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/board/board-routing.module.ts ***!
  \***********************************************/
/*! exports provided: BoardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardRoutingModule", function() { return BoardRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/board/dashboard/dashboard.component.ts");
/* harmony import */ var _navprofile_navprofile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./navprofile/navprofile.component */ "./src/app/board/navprofile/navprofile.component.ts");
/* harmony import */ var _profile_viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile/viewprofile/viewprofile.component */ "./src/app/board/profile/viewprofile/viewprofile.component.ts");
/* harmony import */ var _profile_editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile/editprofile/editprofile.component */ "./src/app/board/profile/editprofile/editprofile.component.ts");
/* harmony import */ var _addhouse_addhouse_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addhouse/addhouse.component */ "./src/app/board/addhouse/addhouse.component.ts");
/* harmony import */ var _viewhouse_viewhouse_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./viewhouse/viewhouse.component */ "./src/app/board/viewhouse/viewhouse.component.ts");
/* harmony import */ var _viewclient_viewclient_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./viewclient/viewclient.component */ "./src/app/board/viewclient/viewclient.component.ts");
/* harmony import */ var _payments_payments_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./payments/payments.component */ "./src/app/board/payments/payments.component.ts");
/* harmony import */ var _payments_add_add_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./payments/add/add.component */ "./src/app/board/payments/add/add.component.ts");
/* harmony import */ var _payments_view_view_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./payments/view/view.component */ "./src/app/board/payments/view/view.component.ts");
/* harmony import */ var _payments_history_history_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./payments/history/history.component */ "./src/app/board/payments/history/history.component.ts");
/* harmony import */ var _myrequests_myrequests_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./myrequests/myrequests.component */ "./src/app/board/myrequests/myrequests.component.ts");















const routes = [
    {
        path: 'dashboard',
        component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"],
        children: [{
                path: 'profile',
                component: _navprofile_navprofile_component__WEBPACK_IMPORTED_MODULE_4__["NavprofileComponent"]
            },
            {
                path: 'viewprofile',
                component: _profile_viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_5__["ViewprofileComponent"]
            },
            {
                path: 'editprofile',
                component: _profile_editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_6__["EditprofileComponent"]
            },
            {
                path: 'addhouse',
                component: _addhouse_addhouse_component__WEBPACK_IMPORTED_MODULE_7__["AddhouseComponent"]
            },
            {
                path: 'viewhouse',
                component: _viewhouse_viewhouse_component__WEBPACK_IMPORTED_MODULE_8__["ViewhouseComponent"]
            },
            {
                path: 'viewclient',
                component: _viewclient_viewclient_component__WEBPACK_IMPORTED_MODULE_9__["ViewclientComponent"]
            },
            {
                path: 'payments',
                component: _payments_payments_component__WEBPACK_IMPORTED_MODULE_10__["PaymentsComponent"]
            },
            {
                path: 'addpayment',
                component: _payments_add_add_component__WEBPACK_IMPORTED_MODULE_11__["AddComponent"]
            },
            {
                path: 'viewpayment',
                component: _payments_view_view_component__WEBPACK_IMPORTED_MODULE_12__["ViewComponent"]
            },
            {
                path: 'viewpaymenthistory',
                component: _payments_history_history_component__WEBPACK_IMPORTED_MODULE_13__["HistoryComponent"]
            },
            {
                path: 'myrequests',
                component: _myrequests_myrequests_component__WEBPACK_IMPORTED_MODULE_14__["MyrequestsComponent"]
            }
        ]
    }
];
let BoardRoutingModule = class BoardRoutingModule {
};
BoardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], BoardRoutingModule);



/***/ }),

/***/ "./src/app/board/board.module.ts":
/*!***************************************!*\
  !*** ./src/app/board/board.module.ts ***!
  \***************************************/
/*! exports provided: BoardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardModule", function() { return BoardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _board_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./board-routing.module */ "./src/app/board/board-routing.module.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/board/dashboard/dashboard.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/board/profile/profile.component.ts");
/* harmony import */ var _profile_viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile/viewprofile/viewprofile.component */ "./src/app/board/profile/viewprofile/viewprofile.component.ts");
/* harmony import */ var _profile_editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile/editprofile/editprofile.component */ "./src/app/board/profile/editprofile/editprofile.component.ts");
/* harmony import */ var _navprofile_navprofile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navprofile/navprofile.component */ "./src/app/board/navprofile/navprofile.component.ts");
/* harmony import */ var _addhouse_addhouse_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./addhouse/addhouse.component */ "./src/app/board/addhouse/addhouse.component.ts");
/* harmony import */ var _viewhouse_viewhouse_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./viewhouse/viewhouse.component */ "./src/app/board/viewhouse/viewhouse.component.ts");
/* harmony import */ var _viewclient_viewclient_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./viewclient/viewclient.component */ "./src/app/board/viewclient/viewclient.component.ts");
/* harmony import */ var _payments_payments_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./payments/payments.component */ "./src/app/board/payments/payments.component.ts");
/* harmony import */ var _payments_nav_nav_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./payments/nav/nav.component */ "./src/app/board/payments/nav/nav.component.ts");
/* harmony import */ var _payments_add_add_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./payments/add/add.component */ "./src/app/board/payments/add/add.component.ts");
/* harmony import */ var _payments_view_view_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./payments/view/view.component */ "./src/app/board/payments/view/view.component.ts");
/* harmony import */ var _payments_history_history_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./payments/history/history.component */ "./src/app/board/payments/history/history.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _myrequests_myrequests_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./myrequests/myrequests.component */ "./src/app/board/myrequests/myrequests.component.ts");



















let BoardModule = class BoardModule {
};
BoardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"], _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__["ProfileComponent"], _profile_viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_6__["ViewprofileComponent"], _profile_editprofile_editprofile_component__WEBPACK_IMPORTED_MODULE_7__["EditprofileComponent"], _navprofile_navprofile_component__WEBPACK_IMPORTED_MODULE_8__["NavprofileComponent"], _addhouse_addhouse_component__WEBPACK_IMPORTED_MODULE_9__["AddhouseComponent"], _viewhouse_viewhouse_component__WEBPACK_IMPORTED_MODULE_10__["ViewhouseComponent"], _viewclient_viewclient_component__WEBPACK_IMPORTED_MODULE_11__["ViewclientComponent"], _payments_payments_component__WEBPACK_IMPORTED_MODULE_12__["PaymentsComponent"], _payments_nav_nav_component__WEBPACK_IMPORTED_MODULE_13__["NavComponent"], _payments_add_add_component__WEBPACK_IMPORTED_MODULE_14__["AddComponent"], _payments_view_view_component__WEBPACK_IMPORTED_MODULE_15__["ViewComponent"], _payments_history_history_component__WEBPACK_IMPORTED_MODULE_16__["HistoryComponent"], _myrequests_myrequests_component__WEBPACK_IMPORTED_MODULE_18__["MyrequestsComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _board_routing_module__WEBPACK_IMPORTED_MODULE_3__["BoardRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"]
        ]
    })
], BoardModule);



/***/ }),

/***/ "./src/app/board/dashboard/dashboard.component.css":
/*!*********************************************************!*\
  !*** ./src/app/board/dashboard/dashboard.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/dashboard/dashboard.component.ts":
/*!********************************************************!*\
  !*** ./src/app/board/dashboard/dashboard.component.ts ***!
  \********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let DashboardComponent = class DashboardComponent {
    constructor(d) {
        this.d = d;
    }
    ngOnInit() {
    }
    logout() {
        localStorage.removeItem('idToken');
        this.d.navigate(['/nav/carousel']);
    }
};
DashboardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/dashboard/dashboard.component.html"),
        styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/board/dashboard/dashboard.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], DashboardComponent);



/***/ }),

/***/ "./src/app/board/myrequests/myrequests.component.css":
/*!***********************************************************!*\
  !*** ./src/app/board/myrequests/myrequests.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL215cmVxdWVzdHMvbXlyZXF1ZXN0cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/myrequests/myrequests.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/board/myrequests/myrequests.component.ts ***!
  \**********************************************************/
/*! exports provided: MyrequestsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyrequestsComponent", function() { return MyrequestsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let MyrequestsComponent = class MyrequestsComponent {
    constructor(http, register) {
        this.http = http;
        this.register = register;
    }
    ngOnInit() {
        this.http.get(`dashboard/myrequests/${this.register.currentuser[0].username}`).subscribe(res => {
            this.currentuser = res['data'];
        });
        console.log(this.currentuser);
        console.log(this.register.currentuser[0].username);
    }
};
MyrequestsComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
MyrequestsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-myrequests',
        template: __webpack_require__(/*! raw-loader!./myrequests.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/myrequests/myrequests.component.html"),
        styles: [__webpack_require__(/*! ./myrequests.component.css */ "./src/app/board/myrequests/myrequests.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], MyrequestsComponent);



/***/ }),

/***/ "./src/app/board/navprofile/navprofile.component.css":
/*!***********************************************************!*\
  !*** ./src/app/board/navprofile/navprofile.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL25hdnByb2ZpbGUvbmF2cHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/navprofile/navprofile.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/board/navprofile/navprofile.component.ts ***!
  \**********************************************************/
/*! exports provided: NavprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavprofileComponent", function() { return NavprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavprofileComponent = class NavprofileComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navprofile',
        template: __webpack_require__(/*! raw-loader!./navprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/navprofile/navprofile.component.html"),
        styles: [__webpack_require__(/*! ./navprofile.component.css */ "./src/app/board/navprofile/navprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavprofileComponent);



/***/ }),

/***/ "./src/app/board/payments/add/add.component.css":
/*!******************************************************!*\
  !*** ./src/app/board/payments/add/add.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3BheW1lbnRzL2FkZC9hZGQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/payments/add/add.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/board/payments/add/add.component.ts ***!
  \*****************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let AddComponent = class AddComponent {
    constructor(http, hc) {
        this.http = http;
        this.hc = hc;
    }
    ngOnInit() {
    }
    addpay(data) {
        if (data.account == "" || data.ifsc == "") {
            alert("account and ifsc fields are mandatory");
        }
        else {
            //console.log(data)
            data.username = this.hc.currentuser[0].username;
            this.http.post('/dashboard/addpayments', data).subscribe((res) => {
                alert(res["message"]);
            });
        }
    }
};
AddComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add',
        template: __webpack_require__(/*! raw-loader!./add.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/payments/add/add.component.html"),
        styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/board/payments/add/add.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], AddComponent);



/***/ }),

/***/ "./src/app/board/payments/history/history.component.css":
/*!**************************************************************!*\
  !*** ./src/app/board/payments/history/history.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3BheW1lbnRzL2hpc3RvcnkvaGlzdG9yeS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/payments/history/history.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/board/payments/history/history.component.ts ***!
  \*************************************************************/
/*! exports provided: HistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryComponent", function() { return HistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let HistoryComponent = class HistoryComponent {
    constructor(http, register) {
        this.http = http;
        this.register = register;
        this.clients = [];
    }
    ngOnInit() {
        this.http.get(`/dashboard/viewpaymenthistory/${this.register.currentuser[0].username}`).subscribe(res => {
            this.clients = res['data'];
        });
        // console.log(this.clients)
        console.log(this.register.currentuser[0].username);
    }
};
HistoryComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"] }
];
HistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history',
        template: __webpack_require__(/*! raw-loader!./history.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/payments/history/history.component.html"),
        styles: [__webpack_require__(/*! ./history.component.css */ "./src/app/board/payments/history/history.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"]])
], HistoryComponent);



/***/ }),

/***/ "./src/app/board/payments/nav/nav.component.css":
/*!******************************************************!*\
  !*** ./src/app/board/payments/nav/nav.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3BheW1lbnRzL25hdi9uYXYuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/payments/nav/nav.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/board/payments/nav/nav.component.ts ***!
  \*****************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavComponent = class NavComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav',
        template: __webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/payments/nav/nav.component.html"),
        styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/board/payments/nav/nav.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavComponent);



/***/ }),

/***/ "./src/app/board/payments/payments.component.css":
/*!*******************************************************!*\
  !*** ./src/app/board/payments/payments.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3BheW1lbnRzL3BheW1lbnRzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/board/payments/payments.component.ts":
/*!******************************************************!*\
  !*** ./src/app/board/payments/payments.component.ts ***!
  \******************************************************/
/*! exports provided: PaymentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsComponent", function() { return PaymentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PaymentsComponent = class PaymentsComponent {
    constructor() { }
    ngOnInit() {
    }
};
PaymentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payments',
        template: __webpack_require__(/*! raw-loader!./payments.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/payments/payments.component.html"),
        styles: [__webpack_require__(/*! ./payments.component.css */ "./src/app/board/payments/payments.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PaymentsComponent);



/***/ }),

/***/ "./src/app/board/payments/view/view.component.css":
/*!********************************************************!*\
  !*** ./src/app/board/payments/view/view.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3BheW1lbnRzL3ZpZXcvdmlldy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/payments/view/view.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/board/payments/view/view.component.ts ***!
  \*******************************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let ViewComponent = class ViewComponent {
    constructor(hc, http) {
        this.hc = hc;
        this.http = http;
    }
    ngOnInit() {
        var username = this.hc.currentuser[0].username;
        this.http.get(`/dashboard/viewpayments/${username}`).subscribe(res => {
            this.payment = res['message'];
        });
        //console.log(this.payment)
    }
};
ViewComponent.ctorParameters = () => [
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view',
        template: __webpack_require__(/*! raw-loader!./view.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/payments/view/view.component.html"),
        styles: [__webpack_require__(/*! ./view.component.css */ "./src/app/board/payments/view/view.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], ViewComponent);



/***/ }),

/***/ "./src/app/board/profile/editprofile/editprofile.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/board/profile/editprofile/editprofile.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3Byb2ZpbGUvZWRpdHByb2ZpbGUvZWRpdHByb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/profile/editprofile/editprofile.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/board/profile/editprofile/editprofile.component.ts ***!
  \********************************************************************/
/*! exports provided: EditprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditprofileComponent", function() { return EditprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let EditprofileComponent = class EditprofileComponent {
    constructor(cm, http, router) {
        this.cm = cm;
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
        this.currentuser = this.cm.currentuser[0];
        console.log(this.cm.currentuser);
    }
    edit(data) {
        if (data.number == "" || data.upaasword == "") {
            alert("mobileno & password are mandatory");
        }
        else {
            //console.log(data);
            this.http.put('dashboard/editprofile', data).subscribe(res => {
                alert(res["message"]);
                this.router.navigate(['/dashboard/viewprofile']);
            });
        }
    }
};
EditprofileComponent.ctorParameters = () => [
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
EditprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editprofile',
        template: __webpack_require__(/*! raw-loader!./editprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/profile/editprofile/editprofile.component.html"),
        styles: [__webpack_require__(/*! ./editprofile.component.css */ "./src/app/board/profile/editprofile/editprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], EditprofileComponent);



/***/ }),

/***/ "./src/app/board/profile/profile.component.css":
/*!*****************************************************!*\
  !*** ./src/app/board/profile/profile.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/profile/profile.component.ts":
/*!****************************************************!*\
  !*** ./src/app/board/profile/profile.component.ts ***!
  \****************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProfileComponent = class ProfileComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/profile/profile.component.html"),
        styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/board/profile/profile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProfileComponent);



/***/ }),

/***/ "./src/app/board/profile/viewprofile/viewprofile.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/board/profile/viewprofile/viewprofile.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3Byb2ZpbGUvdmlld3Byb2ZpbGUvdmlld3Byb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/profile/viewprofile/viewprofile.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/board/profile/viewprofile/viewprofile.component.ts ***!
  \********************************************************************/
/*! exports provided: ViewprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewprofileComponent", function() { return ViewprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let ViewprofileComponent = class ViewprofileComponent {
    constructor(cm, http) {
        this.cm = cm;
        this.http = http;
    }
    ngOnInit() {
        this.currentuser = this.cm.currentuser[0];
        //console.log(this.currentuser)
    }
};
ViewprofileComponent.ctorParameters = () => [
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
ViewprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewprofile',
        template: __webpack_require__(/*! raw-loader!./viewprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/profile/viewprofile/viewprofile.component.html"),
        styles: [__webpack_require__(/*! ./viewprofile.component.css */ "./src/app/board/profile/viewprofile/viewprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], ViewprofileComponent);



/***/ }),

/***/ "./src/app/board/viewclient/viewclient.component.css":
/*!***********************************************************!*\
  !*** ./src/app/board/viewclient/viewclient.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3ZpZXdjbGllbnQvdmlld2NsaWVudC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/board/viewclient/viewclient.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/board/viewclient/viewclient.component.ts ***!
  \**********************************************************/
/*! exports provided: ViewclientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewclientComponent", function() { return ViewclientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let ViewclientComponent = class ViewclientComponent {
    constructor(hc, register) {
        this.hc = hc;
        this.register = register;
    }
    ngOnInit() {
        this.hc.get(`/dashboard/viewclient/${this.register.currentuser[0].username}`).subscribe(res => {
            this.clients = (res["message"]);
            console.log(this.clients);
        });
        //console.log(this.register.currentuser[0].username)
    }
    accept(data) {
        data.reqstatus = "Request Accepted";
        this.register.setResponse(data).subscribe(res => {
            alert('response sent');
            this.clients = res['data'];
        });
    }
    reject(data) {
        data.reqstatus = "Request Rejected";
        this.register.setResponse(data).subscribe(res => {
            alert('response sent');
            this.clients = res['data'];
        });
    }
};
ViewclientComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
ViewclientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewclient',
        template: __webpack_require__(/*! raw-loader!./viewclient.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/viewclient/viewclient.component.html"),
        styles: [__webpack_require__(/*! ./viewclient.component.css */ "./src/app/board/viewclient/viewclient.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], ViewclientComponent);



/***/ }),

/***/ "./src/app/board/viewhouse/viewhouse.component.css":
/*!*********************************************************!*\
  !*** ./src/app/board/viewhouse/viewhouse.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JvYXJkL3ZpZXdob3VzZS92aWV3aG91c2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/board/viewhouse/viewhouse.component.ts":
/*!********************************************************!*\
  !*** ./src/app/board/viewhouse/viewhouse.component.ts ***!
  \********************************************************/
/*! exports provided: ViewhouseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewhouseComponent", function() { return ViewhouseComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let ViewhouseComponent = class ViewhouseComponent {
    constructor(hc, http) {
        this.hc = hc;
        this.http = http;
    }
    ngOnInit() {
        var username = this.hc.currentuser[0].username;
        this.http.get(`/dashboard/viewhouse/${username}`).subscribe(house => {
            if (house['message'] == 'unauthorized access') {
                alert(house['message']);
            }
            else {
                this.house = house['message'];
            }
        });
        console.log(this.house);
    }
    delete(address) {
        this.http.delete(`/dashboard/delete/${address}`).subscribe(res => {
            alert(res['message']);
            this.house = res['data'];
        });
    }
};
ViewhouseComponent.ctorParameters = () => [
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ViewhouseComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viewhouse',
        template: __webpack_require__(/*! raw-loader!./viewhouse.component.html */ "./node_modules/raw-loader/index.js!./src/app/board/viewhouse/viewhouse.component.html"),
        styles: [__webpack_require__(/*! ./viewhouse.component.css */ "./src/app/board/viewhouse/viewhouse.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], ViewhouseComponent);



/***/ }),

/***/ "./src/app/carousel/carousel.component.css":
/*!*************************************************!*\
  !*** ./src/app/carousel/carousel.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nhcm91c2VsL2Nhcm91c2VsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/carousel/carousel.component.ts":
/*!************************************************!*\
  !*** ./src/app/carousel/carousel.component.ts ***!
  \************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CarouselComponent = class CarouselComponent {
    constructor() { }
    ngOnInit() {
    }
};
CarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-carousel',
        template: __webpack_require__(/*! raw-loader!./carousel.component.html */ "./node_modules/raw-loader/index.js!./src/app/carousel/carousel.component.html"),
        styles: [__webpack_require__(/*! ./carousel.component.css */ "./src/app/carousel/carousel.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CarouselComponent);



/***/ }),

/***/ "./src/app/changepassword/changepassword.component.css":
/*!*************************************************************!*\
  !*** ./src/app/changepassword/changepassword.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYW5nZXBhc3N3b3JkL2NoYW5nZXBhc3N3b3JkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/changepassword/changepassword.component.ts":
/*!************************************************************!*\
  !*** ./src/app/changepassword/changepassword.component.ts ***!
  \************************************************************/
/*! exports provided: ChangepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangepasswordComponent", function() { return ChangepasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ChangepasswordComponent = class ChangepasswordComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
    }
    changepassword(x) {
        this.http.put('nav/changepassword', x).subscribe(res => {
            alert(res['message']);
            this.router.navigate(['nav/login']);
        });
    }
};
ChangepasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ChangepasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-changepassword',
        template: __webpack_require__(/*! raw-loader!./changepassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/changepassword/changepassword.component.html"),
        styles: [__webpack_require__(/*! ./changepassword.component.css */ "./src/app/changepassword/changepassword.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ChangepasswordComponent);



/***/ }),

/***/ "./src/app/contactus/contactus.component.css":
/*!***************************************************!*\
  !*** ./src/app/contactus/contactus.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3R1cy9jb250YWN0dXMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/contactus/contactus.component.ts":
/*!**************************************************!*\
  !*** ./src/app/contactus/contactus.component.ts ***!
  \**************************************************/
/*! exports provided: ContactusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusComponent", function() { return ContactusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ContactusComponent = class ContactusComponent {
    constructor() { }
    ngOnInit() {
    }
};
ContactusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contactus',
        template: __webpack_require__(/*! raw-loader!./contactus.component.html */ "./node_modules/raw-loader/index.js!./src/app/contactus/contactus.component.html"),
        styles: [__webpack_require__(/*! ./contactus.component.css */ "./src/app/contactus/contactus.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContactusComponent);



/***/ }),

/***/ "./src/app/forgotpassword/forgotpassword.component.css":
/*!*************************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZvcmdvdHBhc3N3b3JkL2ZvcmdvdHBhc3N3b3JkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/forgotpassword/forgotpassword.component.ts":
/*!************************************************************!*\
  !*** ./src/app/forgotpassword/forgotpassword.component.ts ***!
  \************************************************************/
/*! exports provided: ForgotpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordComponent", function() { return ForgotpasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ForgotpasswordComponent = class ForgotpasswordComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
    }
    password(x) {
        this.http.post('nav/forgotpassword', x).subscribe(res => {
            alert(res["message"]);
            if (res['message'] == "user found") {
                this.router.navigate(['nav/otp']);
            }
            else {
                this.router.navigate(['nav/forgotpassword']);
            }
        });
    }
};
ForgotpasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ForgotpasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgotpassword',
        template: __webpack_require__(/*! raw-loader!./forgotpassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/forgotpassword/forgotpassword.component.html"),
        styles: [__webpack_require__(/*! ./forgotpassword.component.css */ "./src/app/forgotpassword/forgotpassword.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ForgotpasswordComponent);



/***/ }),

/***/ "./src/app/house.service.ts":
/*!**********************************!*\
  !*** ./src/app/house.service.ts ***!
  \**********************************/
/*! exports provided: HouseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HouseService", function() { return HouseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let HouseService = class HouseService {
    constructor(http) {
        this.http = http;
    }
    request() {
        return this.http.get('dashboard/viewhouse');
    }
    read() {
        return this.http.get('dashboard/viewpayments');
    }
    history() {
        return this.http.get('home/paymentshistory');
    }
    pay() {
        return this.http.get('home/viewpaymentshistory');
    }
};
HouseService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HouseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], HouseService);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");





let LoginComponent = class LoginComponent {
    constructor(ds, rc, cm) {
        this.ds = ds;
        this.rc = rc;
        this.cm = cm;
    }
    ngOnInit() {
    }
    method(data) {
        if (data.username == '' || data.password == '' || data.user == '') {
            alert('username,password,usertype are mandatory');
        }
        else {
            this.rc.post('nav/login', data).subscribe((res) => {
                //console.log(res["message"]);
                if (res["message"] == "Invalid owner name") {
                    alert("please enter valid owner name");
                }
                else if (res["message"] == "Invalid owner password") {
                    alert("please enter valid password");
                }
                else if (res["message"] == "owner success") {
                    alert("owner successfully login..");
                    localStorage.setItem("idToken", res['token']);
                    //console.log(res['token']);
                    //console.log(res['userdata'])
                    this.cm.currentuser = res['userdata'];
                    this.ds.navigate(['/dashboard/viewprofile']);
                }
                else if (res["message"] == "Invalid vendor name") {
                    alert("please enter valid vendor name");
                }
                else if (res["message"] == "Invalid vendor password") {
                    alert("please enter valid password");
                }
                else if (res["message"] == "vendor success") {
                    alert("vendor successfully login");
                    localStorage.setItem("idToken", res['token']);
                    //console.log(res['token']);
                    //console.log(res['userdata'])
                    this.cm.currentuser = res['userdata'];
                    this.ds.navigate(['/home/viprofile']);
                }
            });
        }
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi9uYXYuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavComponent = class NavComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav',
        template: __webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html"),
        styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavComponent);



/***/ }),

/***/ "./src/app/otp/otp.component.css":
/*!***************************************!*\
  !*** ./src/app/otp/otp.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL290cC9vdHAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/otp/otp.component.ts":
/*!**************************************!*\
  !*** ./src/app/otp/otp.component.ts ***!
  \**************************************/
/*! exports provided: OtpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpComponent", function() { return OtpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let OtpComponent = class OtpComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
    }
    otp(x) {
        this.http.post('nav/otp', x).subscribe(res => {
            alert(res["message"]);
            if (res['message'] == "verifiedOTP") {
                this.router.navigate(['nav/changepassword']);
            }
            else {
                this.router.navigate(['nav/otp']);
            }
        });
    }
};
OtpComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
OtpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-otp',
        template: __webpack_require__(/*! raw-loader!./otp.component.html */ "./node_modules/raw-loader/index.js!./src/app/otp/otp.component.html"),
        styles: [__webpack_require__(/*! ./otp.component.css */ "./src/app/otp/otp.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], OtpComponent);



/***/ }),

/***/ "./src/app/register.service.ts":
/*!*************************************!*\
  !*** ./src/app/register.service.ts ***!
  \*************************************/
/*! exports provided: RegisterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterService", function() { return RegisterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let RegisterService = class RegisterService {
    constructor(hc) {
        this.hc = hc;
    }
    setResponse(data) {
        return this.hc.put('dashboard/viewclient', data);
    }
    deleteClient(data) {
        return this.hc.delete('/dashboard/viewclient', data);
    }
};
RegisterService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
RegisterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], RegisterService);



/***/ }),

/***/ "./src/app/register/register.component.css":
/*!*************************************************!*\
  !*** ./src/app/register/register.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let RegisterComponent = class RegisterComponent {
    constructor(c, http) {
        this.c = c;
        this.http = http;
    }
    ngOnInit() {
    }
    Change(data) {
        if (data.username == '' || data.upassword == '' || data.user == '') {
            alert('username,password,usertype are mandatory');
        }
        else {
            //console.log(data)
            this.http.post('/nav/register/', data).subscribe((res) => {
                if (res["message"] == "name is already exist") {
                    alert("username is already exists");
                }
                else if (res["message"] == "successfully registered") {
                    alert("regstered successfully..");
                    this.c.navigate(['/nav/login']);
                }
            });
        }
    }
};
RegisterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: __webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/index.js!./src/app/register/register.component.html"),
        styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], RegisterComponent);



/***/ }),

/***/ "./src/app/status.service.ts":
/*!***********************************!*\
  !*** ./src/app/status.service.ts ***!
  \***********************************/
/*! exports provided: StatusService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusService", function() { return StatusService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let StatusService = class StatusService {
    constructor() { }
};
StatusService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], StatusService);



/***/ }),

/***/ "./src/app/tenanet/home/home.component.css":
/*!*************************************************!*\
  !*** ./src/app/tenanet/home/home.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/tenanet/home/home.component.ts":
/*!************************************************!*\
  !*** ./src/app/tenanet/home/home.component.ts ***!
  \************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let HomeComponent = class HomeComponent {
    constructor(g) {
        this.g = g;
    }
    ngOnInit() {
    }
    logout() {
        localStorage.removeItem('idToken');
        this.g.navigate(['/nav/carousel']);
    }
};
HomeComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/tenanet/home/home.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], HomeComponent);



/***/ }),

/***/ "./src/app/tenanet/payments/dopay/dopay.component.css":
/*!************************************************************!*\
  !*** ./src/app/tenanet/payments/dopay/dopay.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcGF5bWVudHMvZG9wYXkvZG9wYXkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/tenanet/payments/dopay/dopay.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/tenanet/payments/dopay/dopay.component.ts ***!
  \***********************************************************/
/*! exports provided: DopayComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DopayComponent", function() { return DopayComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");




let DopayComponent = class DopayComponent {
    constructor(ds, register) {
        this.ds = ds;
        this.register = register;
    }
    ngOnInit() {
    }
    dopay(data) {
        if (data.owner == "" || data.address1 == "" || data.month == "" || data.amount == "" || data.acno == "" || data.code == "") {
            alert("all fields are mandatory");
        }
        else {
            //console.log(data)
            data.vendorname = this.register.currentuser[0].username;
            data.paystatus = "paid";
            this.ds.post('/home/dopay', data).subscribe((res) => {
                alert(res["message"]);
            });
        }
    }
};
DopayComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"] }
];
DopayComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dopay',
        template: __webpack_require__(/*! raw-loader!./dopay.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/dopay/dopay.component.html"),
        styles: [__webpack_require__(/*! ./dopay.component.css */ "./src/app/tenanet/payments/dopay/dopay.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]])
], DopayComponent);



/***/ }),

/***/ "./src/app/tenanet/payments/nav/nav.component.css":
/*!********************************************************!*\
  !*** ./src/app/tenanet/payments/nav/nav.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcGF5bWVudHMvbmF2L25hdi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/tenanet/payments/nav/nav.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/tenanet/payments/nav/nav.component.ts ***!
  \*******************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NavComponent = class NavComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav',
        template: __webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/nav/nav.component.html"),
        styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/tenanet/payments/nav/nav.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], NavComponent);



/***/ }),

/***/ "./src/app/tenanet/payments/paymenthistory/paymenthistory.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/tenanet/payments/paymenthistory/paymenthistory.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcGF5bWVudHMvcGF5bWVudGhpc3RvcnkvcGF5bWVudGhpc3RvcnkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/tenanet/payments/paymenthistory/paymenthistory.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/tenanet/payments/paymenthistory/paymenthistory.component.ts ***!
  \*****************************************************************************/
/*! exports provided: PaymenthistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymenthistoryComponent", function() { return PaymenthistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_house_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/house.service */ "./src/app/house.service.ts");



let PaymenthistoryComponent = class PaymenthistoryComponent {
    constructor(http) {
        this.http = http;
    }
    ngOnInit() {
        this.http.history().subscribe(data => {
            this.viewhistory = data['message'];
        });
    }
};
PaymenthistoryComponent.ctorParameters = () => [
    { type: src_app_house_service__WEBPACK_IMPORTED_MODULE_2__["HouseService"] }
];
PaymenthistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-paymenthistory',
        template: __webpack_require__(/*! raw-loader!./paymenthistory.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/paymenthistory/paymenthistory.component.html"),
        styles: [__webpack_require__(/*! ./paymenthistory.component.css */ "./src/app/tenanet/payments/paymenthistory/paymenthistory.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_house_service__WEBPACK_IMPORTED_MODULE_2__["HouseService"]])
], PaymenthistoryComponent);



/***/ }),

/***/ "./src/app/tenanet/payments/payments.component.css":
/*!*********************************************************!*\
  !*** ./src/app/tenanet/payments/payments.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcGF5bWVudHMvcGF5bWVudHMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/tenanet/payments/payments.component.ts":
/*!********************************************************!*\
  !*** ./src/app/tenanet/payments/payments.component.ts ***!
  \********************************************************/
/*! exports provided: PaymentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsComponent", function() { return PaymentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PaymentsComponent = class PaymentsComponent {
    constructor() { }
    ngOnInit() {
    }
};
PaymentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payments',
        template: __webpack_require__(/*! raw-loader!./payments.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/payments/payments.component.html"),
        styles: [__webpack_require__(/*! ./payments.component.css */ "./src/app/tenanet/payments/payments.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], PaymentsComponent);



/***/ }),

/***/ "./src/app/tenanet/profile/edit/edit.component.css":
/*!*********************************************************!*\
  !*** ./src/app/tenanet/profile/edit/edit.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcHJvZmlsZS9lZGl0L2VkaXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/tenanet/profile/edit/edit.component.ts":
/*!********************************************************!*\
  !*** ./src/app/tenanet/profile/edit/edit.component.ts ***!
  \********************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let EditComponent = class EditComponent {
    constructor(http, cm, router) {
        this.http = http;
        this.cm = cm;
        this.router = router;
    }
    ngOnInit() {
        this.currentuser = this.cm.currentuser[0];
        console.log(this.cm.currentuser);
    }
    edit1(data) {
        if (data.number == "" || data.upassword == "") {
            alert("password and mobileno fields are mandatory");
        }
        else {
            this.http.put('home/edit', data).subscribe(res => {
                alert(res["message"]);
                this.router.navigate(['/home/viprofile']);
            });
        }
    }
};
EditComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit',
        template: __webpack_require__(/*! raw-loader!./edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/edit/edit.component.html"),
        styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/tenanet/profile/edit/edit.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], EditComponent);



/***/ }),

/***/ "./src/app/tenanet/profile/profile.component.css":
/*!*******************************************************!*\
  !*** ./src/app/tenanet/profile/profile.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/tenanet/profile/profile.component.ts":
/*!******************************************************!*\
  !*** ./src/app/tenanet/profile/profile.component.ts ***!
  \******************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProfileComponent = class ProfileComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/profile.component.html"),
        styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/tenanet/profile/profile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProfileComponent);



/***/ }),

/***/ "./src/app/tenanet/profile/viprofile/viprofile.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/tenanet/profile/viprofile/viprofile.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvcHJvZmlsZS92aXByb2ZpbGUvdmlwcm9maWxlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/tenanet/profile/viprofile/viprofile.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/tenanet/profile/viprofile/viprofile.component.ts ***!
  \******************************************************************/
/*! exports provided: ViprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViprofileComponent", function() { return ViprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");



let ViprofileComponent = class ViprofileComponent {
    constructor(cm) {
        this.cm = cm;
    }
    ngOnInit() {
        this.currentuser = this.cm.currentuser[0];
        console.log(this.cm.currentuser);
    }
};
ViprofileComponent.ctorParameters = () => [
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"] }
];
ViprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viprofile',
        template: __webpack_require__(/*! raw-loader!./viprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/profile/viprofile/viprofile.component.html"),
        styles: [__webpack_require__(/*! ./viprofile.component.css */ "./src/app/tenanet/profile/viprofile/viprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"]])
], ViprofileComponent);



/***/ }),

/***/ "./src/app/tenanet/search.pipe.ts":
/*!****************************************!*\
  !*** ./src/app/tenanet/search.pipe.ts ***!
  \****************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SearchPipe = class SearchPipe {
    transform(houses, searchWord) {
        if (!searchWord) {
            return houses;
        }
        else {
            return houses.filter(searchedElement => searchedElement.house.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
                searchedElement.address.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
                searchedElement.rent.toString().indexOf(searchWord.toLowerCase()) != -1);
        }
    }
};
SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'search'
    })
], SearchPipe);



/***/ }),

/***/ "./src/app/tenanet/tenanet-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/tenanet/tenanet-routing.module.ts ***!
  \***************************************************/
/*! exports provided: TenanetRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TenanetRoutingModule", function() { return TenanetRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/tenanet/home/home.component.ts");
/* harmony import */ var _payments_payments_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./payments/payments.component */ "./src/app/tenanet/payments/payments.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/tenanet/profile/profile.component.ts");
/* harmony import */ var _whoomtolet_whoomtolet_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./whoomtolet/whoomtolet.component */ "./src/app/tenanet/whoomtolet/whoomtolet.component.ts");
/* harmony import */ var _profile_edit_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile/edit/edit.component */ "./src/app/tenanet/profile/edit/edit.component.ts");
/* harmony import */ var _profile_viprofile_viprofile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile/viprofile/viprofile.component */ "./src/app/tenanet/profile/viprofile/viprofile.component.ts");
/* harmony import */ var _payments_dopay_dopay_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./payments/dopay/dopay.component */ "./src/app/tenanet/payments/dopay/dopay.component.ts");
/* harmony import */ var _payments_paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./payments/paymenthistory/paymenthistory.component */ "./src/app/tenanet/payments/paymenthistory/paymenthistory.component.ts");











const routes = [{
        path: 'home',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
        children: [
            { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__["ProfileComponent"] },
            { path: 'viprofile', component: _profile_viprofile_viprofile_component__WEBPACK_IMPORTED_MODULE_8__["ViprofileComponent"] },
            { path: 'edit', component: _profile_edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"] },
            { path: 'whoomtolet', component: _whoomtolet_whoomtolet_component__WEBPACK_IMPORTED_MODULE_6__["WhoomtoletComponent"] },
            { path: 'payments', component: _payments_payments_component__WEBPACK_IMPORTED_MODULE_4__["PaymentsComponent"] },
            { path: 'dopay', component: _payments_dopay_dopay_component__WEBPACK_IMPORTED_MODULE_9__["DopayComponent"] },
            { path: 'paymenthistory', component: _payments_paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_10__["PaymenthistoryComponent"] },
        ]
    }];
let TenanetRoutingModule = class TenanetRoutingModule {
};
TenanetRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], TenanetRoutingModule);



/***/ }),

/***/ "./src/app/tenanet/tenanet.module.ts":
/*!*******************************************!*\
  !*** ./src/app/tenanet/tenanet.module.ts ***!
  \*******************************************/
/*! exports provided: TenanetModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TenanetModule", function() { return TenanetModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _tenanet_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tenanet-routing.module */ "./src/app/tenanet/tenanet-routing.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/tenanet/home/home.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/tenanet/profile/profile.component.ts");
/* harmony import */ var _whoomtolet_whoomtolet_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./whoomtolet/whoomtolet.component */ "./src/app/tenanet/whoomtolet/whoomtolet.component.ts");
/* harmony import */ var _payments_payments_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./payments/payments.component */ "./src/app/tenanet/payments/payments.component.ts");
/* harmony import */ var _payments_dopay_dopay_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./payments/dopay/dopay.component */ "./src/app/tenanet/payments/dopay/dopay.component.ts");
/* harmony import */ var _payments_nav_nav_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./payments/nav/nav.component */ "./src/app/tenanet/payments/nav/nav.component.ts");
/* harmony import */ var _profile_edit_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./profile/edit/edit.component */ "./src/app/tenanet/profile/edit/edit.component.ts");
/* harmony import */ var _profile_viprofile_viprofile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/viprofile/viprofile.component */ "./src/app/tenanet/profile/viprofile/viprofile.component.ts");
/* harmony import */ var _payments_paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./payments/paymenthistory/paymenthistory.component */ "./src/app/tenanet/payments/paymenthistory/paymenthistory.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _search_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./search.pipe */ "./src/app/tenanet/search.pipe.ts");















let TenanetModule = class TenanetModule {
};
TenanetModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__["ProfileComponent"], _whoomtolet_whoomtolet_component__WEBPACK_IMPORTED_MODULE_6__["WhoomtoletComponent"], _payments_payments_component__WEBPACK_IMPORTED_MODULE_7__["PaymentsComponent"], _payments_dopay_dopay_component__WEBPACK_IMPORTED_MODULE_8__["DopayComponent"], _payments_nav_nav_component__WEBPACK_IMPORTED_MODULE_9__["NavComponent"], _profile_edit_edit_component__WEBPACK_IMPORTED_MODULE_10__["EditComponent"], _profile_viprofile_viprofile_component__WEBPACK_IMPORTED_MODULE_11__["ViprofileComponent"], _payments_paymenthistory_paymenthistory_component__WEBPACK_IMPORTED_MODULE_12__["PaymenthistoryComponent"], _search_pipe__WEBPACK_IMPORTED_MODULE_14__["SearchPipe"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _tenanet_routing_module__WEBPACK_IMPORTED_MODULE_3__["TenanetRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"]
        ]
    })
], TenanetModule);



/***/ }),

/***/ "./src/app/tenanet/whoomtolet/whoomtolet.component.css":
/*!*************************************************************!*\
  !*** ./src/app/tenanet/whoomtolet/whoomtolet.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlbmFuZXQvd2hvb210b2xldC93aG9vbXRvbGV0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/tenanet/whoomtolet/whoomtolet.component.ts":
/*!************************************************************!*\
  !*** ./src/app/tenanet/whoomtolet/whoomtolet.component.ts ***!
  \************************************************************/
/*! exports provided: WhoomtoletComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhoomtoletComponent", function() { return WhoomtoletComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_status_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/status.service */ "./src/app/status.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_register_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/register.service */ "./src/app/register.service.ts");





let WhoomtoletComponent = class WhoomtoletComponent {
    constructor(r, hc, cm) {
        this.r = r;
        this.hc = hc;
        this.cm = cm;
        this.b = true;
    }
    //requestdata()
    //{
    //if(this.status===true)
    // {
    //this.value='request accepted';
    //}
    //else if(this.status===false)
    // {
    // this.value='request rejected';
    // }
    //}
    ngOnInit() {
        //this.status=this.r.status;
        // this.check=this.r.check;
        this.currentuser = this.cm.currentuser[0];
        this.hc.get('/home/whoomtolet').subscribe(res => {
            if (res['message'] == 'unauthorized access') {
                alert(res['message']);
            }
            else {
                this.house = res['message'];
            }
        });
    }
    ngOnchanges() {
    }
    Changestatus(expectedRent, house) {
        // console.log(expectedRent,house);
        // console.log(this.currentuser);
        var intrestedHouse = {
            "eRent": expectedRent,
            "address": house.address,
            "vendorname": this.currentuser.username,
            "number": this.currentuser.number,
            "email": this.currentuser.email,
            "ownername": house.username
        };
        //console.log(intrestedHouse)
        this.hc.post('/home/whoomtolet', intrestedHouse).subscribe(res => {
            alert(res['message']);
        });
        //this.b=false;
        // this.r.whoomtolet=this.b;
    }
};
WhoomtoletComponent.ctorParameters = () => [
    { type: src_app_status_service__WEBPACK_IMPORTED_MODULE_2__["StatusService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_register_service__WEBPACK_IMPORTED_MODULE_4__["RegisterService"] }
];
WhoomtoletComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-whoomtolet',
        template: __webpack_require__(/*! raw-loader!./whoomtolet.component.html */ "./node_modules/raw-loader/index.js!./src/app/tenanet/whoomtolet/whoomtolet.component.html"),
        styles: [__webpack_require__(/*! ./whoomtolet.component.css */ "./src/app/tenanet/whoomtolet/whoomtolet.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_status_service__WEBPACK_IMPORTED_MODULE_2__["StatusService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_register_service__WEBPACK_IMPORTED_MODULE_4__["RegisterService"]])
], WhoomtoletComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/scls-host7/Desktop/prashanth/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map